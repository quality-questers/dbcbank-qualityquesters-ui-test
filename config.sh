#!/bin/bash

# Define o diretório de recursos
resources_dir="src/main/resources"

# Cria o diretório se não existir
mkdir -p $resources_dir

# Escreve as propriedades no arquivo application.properties
#!/bin/bash

echo "valid.username=$VALID_USERNAME" >> src/main/resources/application.properties
echo "valid.password=$VALID_PASSWORD" >> src/main/resources/application.properties
echo "invalid.username=$INVALID_USERNAME" >> src/main/resources/application.properties
echo "invalid.password=$INVALID_PASSWORD" >> src/main/resources/application.properties
echo "blank.username=$BLANK_USERNAME" >> src/main/resources/application.properties
echo "blank.password=$BLANK_PASSWORD" >> src/main/resources/application.properties
echo "email=$EMAIL" >> src/main/resources/application.properties

echo "remetente.username=$REMETENTE_USERNAME" >> src/main/resources/application.properties
echo "remetente.password=$VALID_PASSWORD" >> src/main/resources/application.properties
echo "remetente.password.transaction=$VALID_PASSWORD_TRANSACTION" >> src/main/resources/application.properties

echo "destinatario.username=$DESTINATARIO_USERNAME" >> src/main/resources/application.properties
echo "destinatario.password=$VALID_PASSWORD" >> src/main/resources/application.properties
echo "destinatario.password.transaction=$VALID_PASSWORD_TRANSACTION" >> src/main/resources/application.properties

echo "limit.username=$LIMIT_USERNAME" >> src/main/resources/application.properties
echo "limit.password=$VALID_PASSWORD" >> src/main/resources/application.properties
echo "limit.password.transaction=$VALID_PASSWORD_TRANSACTION" >> src/main/resources/application.properties

echo "valid.username.remetente.transferencia.pix=$VALID_USERNAME_REMETENTE_TRANSFERENCIA_PIX" >> src/main/resources/application.properties
echo "valid.password.remetente.transferencia.pix=$VALID_PASSWORD_REMETENTE_TRANSFERENCIA_PIX" >> src/main/resources/application.properties
echo "valid.username.destinatario.transferencia.pix=$VALID_USERNAME_DESTINATARIO_TRANSFERENCIA_PIX" >> src/main/resources/application.properties
echo "valid.password.destinatario.transferencia.pix=$VALID_PASSWORD_DESTINATARIO_TRANSFERENCIA_PIX" >> src/main/resources/application.properties
echo "valid.transaction.password.remetente.transferencia.pix=$VALID_TRANSACTION_PASSWORD_REMETENTE_TRANSFERENCIA_PIX" >> src/main/resources/application.properties
echo "valid.transaction.password.destinatario.transferencia.pix=$VALID_TRANSACTION_PASSWORD_DESTINATARIO_TRANSFERENCIA_PIX" >> src/main/resources/application.properties
echo "destinatario.keys.transferencia.pix=$DESTINATARIO_KEYS_TRANSFERENCIA_PIX" >> src/main/resources/application.properties
echo "valid.admin.username=$ADM_USERNAME" >> src/main/resources/application.properties
echo "valid.admin.password=$ADM_PASSWORD" >> src/main/resources/application.properties