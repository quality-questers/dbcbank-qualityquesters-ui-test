package org.dbcbank.selectors.cliente.register;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public class RegisterSelectors {
    // Messages
    public static final SelenideElement MSG_TELA_CADASTRO = $("#root > div.flex.flex-col.w-full > div.w-full.relative.flex.items-center.justify-center > div > h2"); //CRIE SUA CONTA
    public static final SelenideElement MSG_CADASTRO_INVALIDO = $("ol li");
    public static final SelenideElement MSG_CPF_INVALIDO = $(byText("CPF inválido"));

    // Inputs
    public static final SelenideElement CAMPO_CPF = $("input[name='document']");
    public static final SelenideElement CAMPO_CELULAR = $("input[name='phoneNumber']");
    public static final SelenideElement CAMPO_EMAIL = $("input[name='email']");
    public static final SelenideElement CAMPO_CONFIRMAR_EMAIL = $("input[name='emailConfirmation']");
    public static final SelenideElement CAMPO_NOME_COMPLETO = $("input[name='name']");
    public static final SelenideElement CAMPO_SENHA = $("input[name='loginPwd']");
    public static final SelenideElement CAMPO_CONFIRMAR_SENHA = $("input[name='loginPwdConfirmation']");
    public static final SelenideElement CAMPO_SENHA_CARTAO = $("input[name='transactionPwd']");
    public static final SelenideElement CAMPO_CONFIRMAR_SENHA_CARTAO = $("input[name='transactionPwdConfirmation']");
    public static final SelenideElement CAMPO_DATA_NASCIMENTO = $("input[name='birthdate']");

    // Buttons e Checkbox
    public static final SelenideElement BTN_PROXIMO = $("button");
    public static final SelenideElement BTN_CRIAR_CONTA = $("[type='submit']");
    public static final SelenideElement BTN_VOLTAR = $(".bg-primary.disabled\\:opacity-50.disabled\\:pointer-events-none.font-medium.gap-2.h-9.inline-flex.items-center.justify-center.px-4.py-2.rounded-md.shadow");
    public static final SelenideElement CHECKBOX_PRIVACIDADE = $("button[role='checkbox']");
    public static final SelenideElement BTN_IR_PARA_LOGIN = $("#root > div.flex.flex-col.w-full > div.w-full.relative.flex.items-center.justify-center > div > button");
    public static final SelenideElement BTN_FECHAR_MODAL_ALERT = $("#root > div:nth-child(2) > ol > li > button");
}
