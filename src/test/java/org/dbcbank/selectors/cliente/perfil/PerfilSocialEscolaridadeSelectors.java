package org.dbcbank.selectors.cliente.perfil;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public class PerfilSocialEscolaridadeSelectors {
    public static final SelenideElement BTN_SOCIAL_ESCOLARIDADE = $(byText("Social e escolaridade"));
    public static final SelenideElement BTN_GRAU_ESCOLARIDADE = $("#root > div.flex-col.justify-between.flex.bg-secondary.min-h-full > div > div.container.pt-8.mt-\\[96px\\] > div > div.items-start.justify-between.flex.gap-4.relative > div.flex-col.flex.bg-white.p-10.rounded-lg.w-full > form > div.grid-cols-2.gap-5.grid > div:nth-child(1) > div > select");
    public static final SelenideElement BTN_REGIME_DE_TRABALHO = $("#root > div.flex-col.justify-between.flex.bg-secondary.min-h-full > div > div.container.pt-8.mt-\\[96px\\] > div > div.items-start.justify-between.flex.gap-4.relative > div.flex-col.flex.bg-white.p-10.rounded-lg.w-full > form > div.grid-cols-2.gap-5.grid > div:nth-child(2) > div > select");
    public static final SelenideElement BTN_TEMPO_DE_PROFISSAO = $("#root > div.flex-col.justify-between.flex.bg-secondary.min-h-full > div > div.container.pt-8.mt-\\[96px\\] > div > div.items-start.justify-between.flex.gap-4.relative > div.flex-col.flex.bg-white.p-10.rounded-lg.w-full > form > div.grid-cols-2.gap-5.grid > div:nth-child(4) > div > select");
    public static final SelenideElement BTN_PROFISSAO = $("input[name='occupation']");
    public static final SelenideElement BTN_SALVAR = $("#root > div.flex-col.justify-between.flex.bg-secondary.min-h-full > div > div.container.pt-8.mt-\\[96px\\] > div > div.items-start.justify-between.flex.gap-4.relative > div.flex-col.flex.bg-white.p-10.rounded-lg.w-full > form > div.justify-end.flex.mt-5 > button");

    public static final SelenideElement INPUT_DESCRICAO = $("input[name='deficitDescription']");
}
