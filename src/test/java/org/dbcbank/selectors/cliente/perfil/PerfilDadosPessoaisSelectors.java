package org.dbcbank.selectors.cliente.perfil;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class PerfilDadosPessoaisSelectors {
    // Inputs
    public static final SelenideElement INPUT_NAME = $("input[name='name']");
    public static final SelenideElement INPUT_DATA_NASCIMENTO = $("input[name='birthdate']");
    public static final SelenideElement INPUT_CPF = $("input[name='cpf']");
    public static final SelenideElement INPUT_CEP = $("input[name='cep']");
    public static final SelenideElement INPUT_BAIRRO = $("input[name='district']");
    public static final SelenideElement INPUT_LOGRADOURO = $("input[name='street']");
    public static final SelenideElement INPUT_NUMERO = $("input[name='number']");
    public static final SelenideElement INPUT_COMPLEMENTO = $("input[name='complement']");
    public static final SelenideElement ABRIR_LISTA_CIDADE = $("[name='cidade']");

    // Buttons
    public static final SelenideElement BTN_SALVAR_DADOS_PESSOAIS = $(".bg-primary.disabled\\:opacity-50.disabled\\:pointer-events-none.font-medium.gap-2.h-9.inline-flex.items-center.justify-center.px-4.py-2.rounded-md.shadow");
}
