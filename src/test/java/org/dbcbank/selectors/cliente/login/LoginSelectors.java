package org.dbcbank.selectors.cliente.login;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class LoginSelectors {
    // Inputs
    public static final SelenideElement CAMPO_CPF = $("input[name='cpf']");
    public static final SelenideElement CAMPO_SENHA = $("input[name='password']");

    // Mensagem
    public static final SelenideElement MSG_MENSAGEM_LOGIN_INVALIDO = $("#root > div:nth-child(2) > ol > li > div > div.text-sm.opacity-90");

    // Btn
    public static final SelenideElement BTN_ACESSAR = $("[type='submit']");
}
