package org.dbcbank.selectors.cartoes;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public class CartoesSelectors {
    public static final SelenideElement BTN_SOLICITAR_CARTAO =  $(byText("Solicitar cartão de crédito"));
    public static final SelenideElement MENSAGEM_DE_SUCESSO_SELETOR =  $(byText("Solicitação feita com sucesso!"));
    public static final SelenideElement BTN_CARTAO_DE_CREDITO =  $(byText("Cartão de crédito"));
    public static final SelenideElement NUMERO_CARTAO =  $("#root > div.flex-col.justify-between.flex.bg-secondary.min-h-full > div > div.container.pt-8.mt-\\[96px\\] > div > div.items-start.justify-between.flex.gap-20 > div.flex-col.flex.bg-white.p-10.rounded-lg.w-full.gap-4 > div.flex.gap-16 > div.flex.flex-col.gap-2 > div.relative > div > span");

}
