package org.dbcbank.selectors.transacoes.boleto;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public class BoletoSelectors {

    private BoletoSelectors() {
        // Classe com métodos estáticos
    }

    // Botões
    public static final SelenideElement BTN_BOLETO = $("a:nth-of-type(2) > .bg-white.cursor-pointer.flex.font-semibold.gap-3.h-\\[44px\\].items-center.px-3.rounded-sm.text-base.text-primary.whitespace-nowrap");
    public static final SelenideElement BTN_BOLETO_POS_PAGAMENTO = $(".active > .bg-primary.cursor-pointer.flex.font-semibold.gap-3.h-\\[44px\\].items-center.px-3.rounded-sm.text-base.text-white.whitespace-nowrap");
    public static final SelenideElement BTN_PAGAR_BOLETO = $("[class] [class='flex-col flex bg-primary text-white w-\\[205px\\] h-\\[124px\\] rounded-sm p-6 gap-6 cursor-pointer']:nth-of-type(1)");
    public static final SelenideElement BTN_GERAR_BOLETO_PRIMEIRA_ETAPA = $(".flex.gap-4.w-full > div:nth-of-type(2)");
    public static final SelenideElement BTN_GERAR_BOLETO_SEGUNDA_ETAPA = $(".flex.gap-3 > button:nth-of-type(1)");
    public static final SelenideElement BTN_GERAR_BOLETO_VENCIDO_SEGUNDA_ETAPA = $(".flex.gap-3 > button:nth-of-type(2)");
    public static final SelenideElement BTN_COPIAR_CODIGO_BARRAS = $("[class='p-6 pt-0 text-slate-950 flex flex-col items-start'] button");
    public static final SelenideElement BTN_CONTINUAR_PAGAMENTO_POS_CODIGO_BARRAS = $("[type='submit']");
    public static final SelenideElement BTN_VOLTAR_PAGINA_ANTERIOR_POS_CRIACAO_BOLETO = $("[class='flex-col items-start flex bg-white p-10 rounded-lg gap-10'] > button:nth-of-type(1)");
    public static final SelenideElement BTN_CONTINUAR_PAGAMENTO_POS_REVISAO_DADOS_BOLETO = $(byText("Continuar pagamento do boleto"));
    public static final SelenideElement BTN_CONTINUAR_PAGAMENTO_POS_ESCOLHA_FORMA_PAGAMENTO = $("[type='submit']");
   public static final SelenideElement BTN_CONTINUAR_PAGAMENTO_POS_INFORMAR_SENHA_PAGAMENTO = $(byText("Confirmar"));


    // Elementos de texto
    public static final SelenideElement MSG_BOLETO_CRIADO_COM_SUCESSO = $(".font-semibold.leading-none.text-2xl.tracking-tight");
    public static final SelenideElement MSG_PAGINA_CODIGO_BARRAS = $(".flex.font-medium.items-center.justify-between.mb-0.text-base.text-primary");
    public static final SelenideElement MSG_VALOR_BOLETO = $("#root > div.flex-col.justify-between.flex.bg-secondary.min-h-full > div > div.container.pt-8.mt-\\[96px\\] > div > div.items-start.justify-between.flex.gap-4.relative > div.flex.flex-col.w-full > div > div.rounded-lg.border.bg-card.text-card-foreground.shadow-sm.col-span-4 > div.p-6.pt-0.text-slate-950.flex.flex-col.items-start > div:nth-child(3) > span.font-semibold");
    public static final SelenideElement MSG_SALDO = $(".flex.gap-4.w-full > div:nth-of-type(2)");
    public static final SelenideElement MSG_BOLETO_INVALIDO = $("#root > div:nth-child(2) > ol > li > div > div.text-sm.font-semibold");
    public static final SelenideElement MSG_PAGINA_REVISAR_INFORMACOES_BOLETO = $(".flex.font-medium.items-center.justify-between.mb-4.text-2xl.text-primary");
    public static final SelenideElement MSG_PAGINAR_REVISAR_INFORMACOES_BOLETO_CONFIRMAR_VALOR_BOLETO = $("#root > div.flex-col.justify-between.flex.bg-secondary.min-h-full > div > div.container.pt-8.mt-\\[96px\\] > div > div.items-start.justify-between.flex.gap-4.relative > div.flex.flex-col.w-full > div > div > div > div > div:nth-child(7) > div > span.text-base.text-slate-900.font-medium");
    public static final SelenideElement MSG_PAGINAR_REVISAR_INFORMACOES_BOLETO_STATUS_BOLETO_PAGO = $(byText("Pago"));
    public static final SelenideElement MSG_ESCOLHER_FORMA_PAGAMENTO = $(".flex.font-medium.items-center.justify-between.mb-4.text-2xl.text-primary");
    public static final SelenideElement MSG_PAGINA_SENHA_TRANSACAO = $(".flex.font-medium.items-center.justify-between.mb-4.text-2xl.text-primary");
    public static final SelenideElement MSG_BOLETO_PAGO_COM_SUCESSO = $(byText("Boleto pago com sucesso"));
    public static final SelenideElement MSG_NUMERO_CODIGO_DE_BARRAS_CRIADO = $("div:nth-of-type(4) > .font-semibold");


    // Inputs
    public static final SelenideElement INPUT_CODIGO_BARRAS = $("input[name='barcode']");
    public static final SelenideElement RADIO_PAGAMENTO_COM_SALDO_EM_CONTA = $("[value='balance']");
    public static final SelenideElement INPUT_DATA_PAGAMENTO = $("button#\\:ru\\:-form-item");
    public static final SelenideElement INPUT_DESCRICAO_PAGAMENTO = $("textarea[name='description']");
}
