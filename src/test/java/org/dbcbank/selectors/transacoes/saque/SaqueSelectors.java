package org.dbcbank.selectors.transacoes.saque;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public class SaqueSelectors {
    public static final SelenideElement BTN_LATERAL_DE_SAQUE = $(byText("Saque"));
    public static final SelenideElement BTN_SAQUE = $(byText("Sacar"));
    public static final SelenideElement INPUT_VALOR_SAQUE = $("input[name='money']");
    public static final SelenideElement BTN_SACAR = $("div#root button[type=\"submit\"]");
    public static final SelenideElement BTN_SUBMIT_SAQUE = $(byText("Confirmar"));
    public static final SelenideElement MENSAGEM_DE_SUCESSO_ELEMENT = $(byText("Saque realizado com sucesso"));
    public static final SelenideElement MENSAGEM_DE_FUNDOS_INSUFICIENTES = $(byText("Fundos insuficientes."));
}
