package org.dbcbank.selectors.transacoes.transferencia.ted;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public class TransferenciaTedSelectors {
    public static final SelenideElement BTN_LATERAL_TRANSFERENCIA = $(byText("Transferência"));
    public static final SelenideElement BTN_TRANSFERENCIA = $(byText("Transferir TED"));
    public static final SelenideElement INPUT_VALOR_TRANSFERENCIA = $("input[name='money']");
    public static final SelenideElement INPUT_ACCOUNT = $("input[name='account']");
    public static final SelenideElement INPUT_BRANCH_ID = $("input[name='agency']");
    public static final SelenideElement INPUT_DIGITO= $("input[name='digit']");
    public static final SelenideElement INPUT_DESCRIPTION= $("textarea[name='description']");
    public static final SelenideElement BTN_SUBMIT_TRANSFERENCIA = $(byText("Continuar transferência"));
    public static final SelenideElement BTN_SUBMIT_CONFIRMAR = $(byText("Confirmar"));
    public static final SelenideElement MENSAGEM_DE_SUCESSO_TRANSFERENCIA_TED = $(byText("Transferência realizada com sucesso"));
    public static final SelenideElement MENSAGEM_ERRO_DE_VALIDACAO = $(byText("Nenhuma conta associada à este número foi encontrada. Verifique se o número da conta foi digitado corretamente no formato 'xxxxx-x'."));
}
