package org.dbcbank.selectors.transacoes.transferencia.pix;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public class TransferenciaPixSelectors {
    public static final SelenideElement BTN_LATERAL_TRANSFERENCIA = $(byText("Transferência"));
    public static final SelenideElement BTN_TRANSFERENCIA = $(byText("Transferir PIX"));
    public static final SelenideElement INPUT_VALOR_TRANSFERENCIA = $("input[name='money']");
    public static final SelenideElement INPUT_ACCOUNT = $("input[name='destinationKey']");
    public static final SelenideElement BTN_SUBMIT_TRANSFERENCIA = $(byText("Continuar transferência"));
    public static final SelenideElement BTN_SUBMIT_CONFIRMAR = $(byText("Confirmar"));
    public static final SelenideElement BTN_CONFIRMAR_REVISAO_DADOS_TRANSFERENCIA = $(byText("Continuar transferência"));
    public static final SelenideElement TITULO_REVISAR_INFORMACOES = $(byText("Revisar informações"));
    public static final SelenideElement TITULO_TRANSFERENCIA_SUCESSO = $("h3");
}
