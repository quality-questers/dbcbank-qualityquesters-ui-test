package org.dbcbank.selectors.transacoes.caixinha;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class SaqueCaixinhaSelectors {
    public static final SelenideElement BTN_GUARDAR_DINHEIRO_CAIXINHA = $("a:nth-of-type(3) > .bg-white.cursor-pointer.flex.font-semibold.gap-3.h-\\[44px\\].items-center.px-3.rounded-sm.text-base.text-primary.whitespace-nowrap");
    public static final SelenideElement BTN_SAQUE_DINHEIRO = $("[class] [class='flex-col flex bg-primary text-white w-\\[205px\\] h-\\[124px\\] rounded-sm p-6 gap-6 cursor-pointer']:nth-of-type(2)");
    public static final SelenideElement BTN_SACAR = $("#root > div.flex-col.justify-between.flex.bg-secondary.min-h-full > div > div.container.pt-8.mt-\\[96px\\] > div > div.items-start.justify-between.flex.gap-4.relative > div.flex.flex-col.w-full > div > div > form > button");

    public static final SelenideElement INPUT_SACAR_DINHEIRO = $("input[name='money']");

    public static final SelenideElement MSG_SAQUE_CAIXINHO_SUCESSO = $("#root > div.flex-col.justify-between.flex.bg-secondary.min-h-full > div > div.container.pt-8.mt-\\[96px\\] > div > div.items-start.justify-between.flex.gap-4.relative > div.flex.flex-col.w-full > div > div > div > h3");
}
