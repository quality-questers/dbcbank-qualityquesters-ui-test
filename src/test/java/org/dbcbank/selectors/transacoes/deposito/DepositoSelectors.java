package org.dbcbank.selectors.transacoes.deposito;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public class DepositoSelectors {
    public static final SelenideElement BTN_DEPOSITO = $(byText("Depósito"));
    public static final SelenideElement INPUT_VALOR = $("input[name='money']");
    public static final SelenideElement NR_CONTA_DESTINATARIO = $("input[name='accNumber']");
    public static final SelenideElement NR_CPF_DESTINATARIO = $("input[name='document']");
    public static final SelenideElement BTN_DEPOSITAR = $("#root > div.flex-col.justify-between.flex.bg-secondary.min-h-full > div > div.container.pt-8.mt-\\[96px\\] > div > div.items-start.justify-between.flex.gap-4.relative > div.flex.flex-col.w-full > div > div.flex.w-full.gap-4 > div");
    public static final SelenideElement BTN_REALIZAR_DEPOSITO = $("#root > div.flex-col.justify-between.flex.bg-secondary.min-h-full > div > div.container.pt-8.mt-\\[96px\\] > div > div.items-start.justify-between.flex.gap-4.relative > div.flex.flex-col.w-full > div > div > form > button");
    public static final SelenideElement BTN_VOLTAR = $("#root > div.flex-col.justify-between.flex.bg-secondary.min-h-full > div > div.container.pt-8.mt-\\[96px\\] > div > div.items-start.justify-between.flex.gap-4.relative > div.flex.flex-col.w-full > div > button");
}
