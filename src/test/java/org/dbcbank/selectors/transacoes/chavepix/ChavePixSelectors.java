package org.dbcbank.selectors.transacoes.chavepix;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public class ChavePixSelectors {
    public static final SelenideElement BTN_CHAVE_PIX = $(byText("Gerênciar PIX"));
    public static final SelenideElement BTN_CRIAR_CHAVE = $(byText("Criar Chave Pix"));
    public static final SelenideElement BTN_CONFIRMAR_CRIACAO_CHAVE = $(byText("Criar chave"));
    public static final SelenideElement SELECT_TIPO_CHAVE = $("[data-testid='tipo-de-chave']");
    public static final SelenideElement TIPO_CHAVE_CRIADA = $("#root > div.flex-col.justify-between.flex.bg-secondary.min-h-full > div > div.container.pt-8.mt-\\[96px\\] > div > div.items-start.justify-between.flex.gap-4.relative > div.flex.flex-col.w-full > div > div > table > tbody > tr > td:nth-child(1)");
    public static final SelenideElement KEY_CHAVE_CRIADA = $("#root > div.flex-col.justify-between.flex.bg-secondary.min-h-full > div > div.container.pt-8.mt-\\[96px\\] > div > div.items-start.justify-between.flex.gap-4.relative > div.flex.flex-col.w-full > div > div > table > tbody > tr > td:nth-child(2)");
}
