package org.dbcbank.test.gerenciadorcartoes.cartoes;

import io.qameta.allure.*;
import org.dbcbank.data.dto.cliente.login.LoginDTO;
import org.dbcbank.data.factory.datafactory.cliente.login.LoginDataFactory;
import org.dbcbank.page.cliente.login.LoginPage;
import org.dbcbank.page.website.cartoes.CartoesPage;
import org.dbcbank.page.website.homepage.HomePage;
import org.dbcbank.rest.controller.UsuarioController;
import org.dbcbank.rest.dto.usuario.RegisterApiModel;
import org.dbcbank.test.BaseTeste;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CartaoDeCreditoTest extends BaseTeste {
    private static final HomePage homePage = new HomePage();
    private static final LoginPage loginPage = new LoginPage();
    private static final CartoesPage cartoesPage = new CartoesPage();

    public void fluxoDeCartao() {
        homePage.clicarNoBtnCartoes();
        cartoesPage.validarUrlDaPagina("https://dbcbank-questers-front-vs3.onrender.com/admin/cards");
    }


    @Test
    @Feature("Criar cartão de crédito")
    @Description("Validar criação de cartão de crédito")
    @Story("Criação de cartão de crédito")
    @Severity(SeverityLevel.NORMAL)
    public void validarCriacaoDeCartaoDeCredito() {
        RegisterApiModel usuario = UsuarioController.cadastrarUsuario();
        loginPage.fazerLogin(new LoginDTO(usuario));

        fluxoDeCartao();
        cartoesPage.clicarBtnSolicitarCartao();

        Assert.assertTrue(cartoesPage.validarMensagemDeSucesso());
    }

    @Test
    @Feature("Listar cartões")
    @Description("Validar a listagem de cartões")
    @Story("Listagem de cartões")
    @Severity(SeverityLevel.NORMAL)
    public void validarListagemDecartoes() {
        loginPage.fazerLogin(LoginDataFactory.loginUsuarioRemetente());

        fluxoDeCartao();
        cartoesPage.clicarBtnCartaoDeCredito();

        Assert.assertTrue(cartoesPage.validarNumeroDoCartao());
    }
}
