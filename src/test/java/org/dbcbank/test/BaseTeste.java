package org.dbcbank.test;

import com.codeborne.selenide.Configuration;
import io.qameta.allure.Description;
import org.dbcbank.util.config.ConfigProperties;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import static com.codeborne.selenide.Selenide.closeWindow;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;


public class BaseTeste {

    @BeforeMethod
    @Description("Abrir navegador antes da execução da suíte de testes")
    public void setUp() {
        initializeConfig();
        openInitialPage();
        maximizeBrowserWindow();
    }

    @AfterMethod
    @Description("Fechar navegador após a execução da suíte de testes")
    public void tearDown() {
        closeWindow();
    }

    private void initializeConfig() {
        ConfigProperties.initializePropertyFile();
        Configuration.browser = "chrome";
        Configuration.headless = true;
        Configuration.timeout = 30000;

        // Configuração para salvar screenshots automaticamente
        Configuration.reportsFolder = "screenshots";
        Configuration.screenshots = false;
    }

    private void openInitialPage() {
        open("https://dbcbank-questers-front-vs3.onrender.com/login");
    }

    private void maximizeBrowserWindow() {
        getWebDriver().manage().window().maximize();
    }
}