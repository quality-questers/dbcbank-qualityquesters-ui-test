package org.dbcbank.test.cliente.register;

import io.qameta.allure.*;
import org.dbcbank.data.dto.cliente.registro.RegisterDTO;
import org.dbcbank.data.factory.datafactory.cliente.registro.RegisterDataFactory;
import org.dbcbank.data.factory.provider.registro.RegisterDataProvider;
import org.dbcbank.page.website.paginainicial.InicialPage;
import org.dbcbank.page.cliente.register.RegisterPage;
import org.dbcbank.test.BaseTeste;
import org.testng.annotations.Test;

public class RegisterTest extends BaseTeste {
    InicialPage inicialPage = new InicialPage();
    RegisterPage registerPage = new RegisterPage();

    @Test
    @Feature("Cadastro de Usuário")
    @Description("Validar cadastro com dados válidos")
    @Story("Cadastro de usuário")
    @Severity(SeverityLevel.NORMAL)
    public void validarCadastroDadosValidos() {
        inicialPage.validarTextoDaHomePage("Conectando você ao mundo das possibilidades");
        inicialPage.clicarNoBtnRegistrar();

        registerPage.verificarUrlDaPagina("https://dbcbank-questers-front-vs3.onrender.com/register");
        registerPage.verificarMensagemNaPaginaDeCadastro();

        RegisterDTO usu = RegisterDataFactory.cadastroDeUsuarioComDadosValidos();

        registerPage.fazerCadastro(usu);
        registerPage.irParaLoginAposCadastro();
    }

    @Test(dataProvider = "TesteNegativosCampoNome", dataProviderClass = RegisterDataProvider.class)
    @Feature("Cadastro de Usuário")
    @Story("Validar campo nome")
    @Description("Teste para validar as regras de negócio do campo nome em cadastro de usuário!")
    @Severity(SeverityLevel.NORMAL)
    public void testValidarCampoNomeNoCadastro(RegisterDTO registro, String mensagemEsperada) {
        inicialPage.clicarNoBtnRegistrar();

        registerPage.fazerCadastro(registro);
        registerPage.verificarMensagemDeCadastroInvalido(mensagemEsperada);
        registerPage.btnFecharModalAlert();
        registerPage.voltarPaginaDeCadastro();
        inicialPage.clicarNoBtnVoltar();
    }

    @Test
    @Feature("Cadastro de Usuário")
    @Description("Validar cadastro com email já cadastrado")
    @Story("Cadastro de usuário")
    @Severity(SeverityLevel.CRITICAL)
    public void testCriarUsuarioComEmailJaUtilizado() {
        inicialPage.clicarNoBtnRegistrar();

        RegisterDTO usu = RegisterDataFactory.cadastroDeUsuarioComDadosValidos();
        registerPage.fazerCadastro(usu);
        registerPage.irParaLoginAposCadastro();
        inicialPage.clicarNoBtnRegistrar();

        RegisterDTO novoUsuario = RegisterDataFactory.usuarioEmail(usu.getEmail());
        registerPage.fazerCadastro(novoUsuario);
        registerPage.verificarMensagemDeCadastroInvalido("Dado(s) já cadastrado(s): email");
    }

    @Test
    @Feature("Cadastro de Usuário")
    @Story("Validar criação de usuário com cpf já em uso")
    @Description("Teste para validar a criação de usuário com cpf já em uso - o sistema deve rejeitar")
    @Severity(SeverityLevel.CRITICAL)
    public void testCriarUsarioComCpfJaUtilizado() {
        inicialPage.clicarNoBtnRegistrar();

        RegisterDTO usu = RegisterDataFactory.cadastroDeUsuarioComDadosValidos();
        registerPage.fazerCadastro(usu);

        registerPage.irParaLoginAposCadastro();
        inicialPage.clicarNoBtnRegistrar();

        RegisterDTO novoUsuario = RegisterDataFactory.usuarioCampoCPF(usu.getCpf());
        registerPage.fazerCadastro(novoUsuario);
        registerPage.verificarMensagemDeCadastroInvalido("Dado(s) já cadastrado(s): document");
        registerPage.btnFecharModalAlert();
    }

    @Test(dataProvider = "TesteNegativosCampoCpf", dataProviderClass = RegisterDataProvider.class)
    @Feature("Cadastro de Usuário")
    @Story("Validar campo CPF")
    @Description("Teste para validar as regras de negócio do campo CPF em cadastro de usuário!")
    @Severity(SeverityLevel.NORMAL)
    public void testValidarCampoCPFNoCadastro(RegisterDTO registro, String mensagemEsperada) {
        inicialPage.clicarNoBtnRegistrar();

        registerPage.fazerCadastroInformandoCpfInvalido(registro);

        registerPage.verificarMensagemDeCpfInvalido(mensagemEsperada);
        inicialPage.clicarNoBtnVoltar();
    }
}
