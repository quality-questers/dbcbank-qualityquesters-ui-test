package org.dbcbank.test.cliente.perfil;

import io.qameta.allure.*;
import org.dbcbank.data.dto.cliente.perfil.PerfilDadosPessoaisDTO;
import org.dbcbank.data.factory.datafactory.cliente.login.LoginDataFactory;
import org.dbcbank.data.factory.datafactory.cliente.perfil.PerfilDadosPessoaisDataFactory;
import org.dbcbank.page.website.homepage.HomePage;
import org.dbcbank.page.cliente.login.LoginPage;
import org.dbcbank.page.cliente.perfil.PerfilDadosPessoaisPage;
import org.dbcbank.test.BaseTeste;
import org.testng.annotations.Test;


public class PerfilDadosPessoaisTest extends BaseTeste {
    HomePage homePage = new HomePage();
    PerfilDadosPessoaisPage perfil = new PerfilDadosPessoaisPage();
    LoginPage loginPage = new LoginPage();
    PerfilDadosPessoaisDataFactory perfilFactory = new PerfilDadosPessoaisDataFactory();

    @Test
    @Feature("Perfil")
    @Description("Validar alteração de Dados Pessoais")
    @Story("Perfil Dados Pessoais")
    @Severity(SeverityLevel.CRITICAL)
    public void validarAlteracaoDeDadosPessoais() {
        loginPage.fazerLogin(LoginDataFactory.loginDadosValidos());
        homePage.clicarNoBtnPerfil();
        perfil.limparCampos();
        PerfilDadosPessoaisDTO perfilDadosPessoais = perfilFactory.alteracaoDeDadosPessoais();
        perfil.alterarDadosPessoais(perfilDadosPessoais);
        perfil.setBtnSalvarDadosPessoais();
        homePage.clicarNoBtnLogout();
    }
}
