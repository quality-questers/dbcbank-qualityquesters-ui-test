package org.dbcbank.test.cliente.perfil;

import io.qameta.allure.*;
import org.dbcbank.data.dto.cliente.perfil.PerfilSocialEscolaridadeDTO;
import org.dbcbank.data.factory.datafactory.cliente.login.LoginDataFactory;
import org.dbcbank.data.factory.datafactory.cliente.perfil.PerfilSocialEscolaridadeDataFactory;
import org.dbcbank.page.website.homepage.HomePage;
import org.dbcbank.page.cliente.login.LoginPage;
import org.dbcbank.page.cliente.perfil.PerfilSocialEscolaridadePage;
import org.dbcbank.test.BaseTeste;
import org.testng.annotations.Test;

public class PerfilSocialEscolaridadeTest extends BaseTeste {
    HomePage homePage = new HomePage();
    PerfilSocialEscolaridadePage perfil = new PerfilSocialEscolaridadePage();
    LoginPage loginPage = new LoginPage();
    PerfilSocialEscolaridadeDataFactory perfilFactory = new PerfilSocialEscolaridadeDataFactory();

    @Test
    @Feature("Perfil")
    @Description("Validar alteração de Social e Escolaridade")
    @Story("Perfil Social e Escolaridade")
    @Severity(SeverityLevel.NORMAL)
    public void validarAlteracaoDePerfilSocialEscolaridade() {
        loginPage.fazerLogin(LoginDataFactory.loginDadosValidos());
        homePage.clicarNoBtnPerfil();
        perfil.clicarBtnSocialEscolaridade();

        PerfilSocialEscolaridadeDTO perfilSocialEscolaridade = perfilFactory.alteracaoSocialEscolaridade();
        perfil.alterarSocialEscolaridade(perfilSocialEscolaridade);
        perfil.clicarBtnSalvar();
        homePage.clicarNoBtnLogout();
    }
}
