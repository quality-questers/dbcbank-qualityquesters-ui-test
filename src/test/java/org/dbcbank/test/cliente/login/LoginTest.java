package org.dbcbank.test.cliente.login;

import io.qameta.allure.*;
import org.dbcbank.data.factory.datafactory.cliente.login.LoginDataFactory;
import org.dbcbank.page.website.homepage.HomePage;
import org.dbcbank.page.cliente.login.LoginPage;
import org.dbcbank.page.website.paginainicial.InicialPage;
import org.dbcbank.test.BaseTeste;
import org.testng.annotations.Test;

public class LoginTest extends BaseTeste {
    HomePage homePage = new HomePage();
    InicialPage inicialPage = new InicialPage();
    LoginPage loginPage = new LoginPage();

    @Test
    @Feature("Login")
    @Description("Validar login com dados válidos")
    @Story("Autenticação de usuário")
    @Severity(SeverityLevel.BLOCKER)
    public void validarLoginComDadosValidos() {

        inicialPage.validarTextoDaHomePage("Conectando você ao mundo das possibilidades");

        loginPage.fazerLogin(LoginDataFactory.loginDadosValidos());
        homePage.clicarNoBtnLogout();
    }
}
