package org.dbcbank.test.transacoes.chavepix;

import io.qameta.allure.*;
import org.dbcbank.data.dto.cliente.login.LoginDTO;
import org.dbcbank.page.transacoes.chavepix.ChavePixPage;
import org.dbcbank.page.website.homepage.HomePage;
import org.dbcbank.page.cliente.login.LoginPage;
import org.dbcbank.rest.controller.UsuarioController;
import org.dbcbank.rest.dto.usuario.RegisterApiModel;
import org.dbcbank.test.BaseTeste;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ChavePixTest extends BaseTeste {
    private final LoginPage loginPage = new LoginPage();
    private final ChavePixPage chavePixPage = new ChavePixPage();
    private RegisterApiModel usuario;
    private final HomePage homePage = new HomePage();

    @BeforeMethod
    public void setup() {
        usuario = UsuarioController.cadastrarUsuario();
    }

    @Test
    @Feature("Gerenciamento de Chave")
    @Description("Validar criação de chave pix com sucesso")
    @Story("Criação de chave pix")
    @Severity(SeverityLevel.CRITICAL)
    public void validarCriacaoDeChavePix() {
        loginPage.fazerLogin(new LoginDTO(usuario));
        homePage.clicarNoBtnAreaFinanceira();
        chavePixPage.clicarNoBtnGerenciaPix();

        chavePixPage.criarChavePix();

        chavePixPage.verificarTipoChaveCriada();

        switch (ChavePixPage.tipoChavePixCriada) {
            case "cpf":
                chavePixPage.verificarKeyPixCriada(usuario.getDocument());
                break;
            case "email":
                chavePixPage.verificarKeyPixCriada(usuario.getEmail());
                break;
            case "telefone":
                chavePixPage.verificarKeyPixCriada(usuario.getPhoneNumber());
                break;
            default:
                break;
        }
    }
}
