package org.dbcbank.test.transacoes.transferencia.pix;

import io.qameta.allure.*;
import org.dbcbank.data.factory.datafactory.cliente.login.LoginDataFactory;
import org.dbcbank.page.website.homepage.HomePage;
import org.dbcbank.page.cliente.login.LoginPage;
import org.dbcbank.page.transacoes.transferencia.pix.TransferenciaPixPage;
import org.dbcbank.test.BaseTeste;
import org.dbcbank.util.user.UserData;
import org.testng.annotations.Test;

public class TransferenciaPixTest extends BaseTeste {
    private final LoginPage loginPage = new LoginPage();
    private final TransferenciaPixPage transferenciaPixPage = new TransferenciaPixPage();
    private final HomePage homePage = new HomePage();

    @Test
    @Feature("Transferencia PIX")
    @Description("Validar transferencia do tipo PIX com sucesso")
    @Story("Transferencia do tipo PIX.")
    @Severity(SeverityLevel.NORMAL)
    public void testValidarTransferenciaTipoPix() {
        loginPage.fazerLogin(LoginDataFactory.loginUsuarioRemetenteTransferenciaPix());
        homePage.clicarNoBtnAreaFinanceira();
        transferenciaPixPage.clicarNoBtnTransferencia();
        transferenciaPixPage.preencherDadosTransferencia();

        transferenciaPixPage.confirmarDadosTransferencia(UserData.senhaTransacaoRemetentePix());

        transferenciaPixPage.validarMensagemDeSucesso("Transferência realizada com sucesso");
    }
}
