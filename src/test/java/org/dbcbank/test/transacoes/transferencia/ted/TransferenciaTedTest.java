package org.dbcbank.test.transacoes.transferencia.ted;

import io.qameta.allure.*;
import org.dbcbank.data.factory.datafactory.cliente.login.LoginDataFactory;
import org.dbcbank.page.cliente.login.LoginPage;
import org.dbcbank.page.transacoes.transferencia.ted.TransferenciaTedPage;
import org.dbcbank.page.website.homepage.HomePage;
import org.dbcbank.test.BaseTeste;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TransferenciaTedTest extends BaseTeste {
    private final HomePage homePage = new HomePage();
    private final LoginPage loginPage = new LoginPage();
    private final TransferenciaTedPage transferenciaTedPage = new TransferenciaTedPage();

    private void fluxoTransferenciaTed() {
        loginPage.fazerLogin(LoginDataFactory.loginUsuarioRemetente());
        homePage.clicarNoBtnAreaFinanceira();
        transferenciaTedPage.clicarNoBtnTransferencia();
        transferenciaTedPage.clicarNoBtnTransferenciaTed();
        transferenciaTedPage.validarUrlDaPagina("https://dbcbank-questers-front-vs3.onrender.com/admin/financas/transferencia/ted");
    }

    @Test
    @Feature("Transferencia TED")
    @Description("Validar transferencia do tipo TED")
    @Story("Transferencia do tipo TED com sucesso.")
    @Severity(SeverityLevel.CRITICAL)
    public void testValidarTransferenciaTipoTED() {
        fluxoTransferenciaTed();
        transferenciaTedPage.realizarTransferencia();

        Assert.assertTrue(transferenciaTedPage.validarMensagemDeSucesso());
    }

    @Test
    @Feature("Transferencia TED")
    @Description("Validar transferencia do tipo TED para conta inválida sem sucesso")
    @Story("Transferencia do tipo TED para conta inválida.")
    @Severity(SeverityLevel.CRITICAL)
    public void testValidarTransferenciaTipoTEDParaContaInvalida() {
        fluxoTransferenciaTed();
        transferenciaTedPage.realizarTransferenciaComContaInvalida();

        Assert.assertTrue(transferenciaTedPage.validarMensagemDeErroDeValidacao());

    }
}
