package org.dbcbank.test.transacoes.limitediario;

import io.qameta.allure.*;
import org.dbcbank.data.factory.datafactory.cliente.login.LoginDataFactory;
import org.dbcbank.page.cliente.login.LoginPage;
import org.dbcbank.page.transacoes.limitediario.LimiteDiarioPage;

import org.dbcbank.page.transacoes.transferencia.ted.TransferenciaTedPage;
import org.dbcbank.page.website.homepage.HomePage;
import org.dbcbank.test.BaseTeste;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;

public class LimiteDiarioTest extends BaseTeste {

    private static final LoginPage loginPage = new LoginPage();
    private static final LimiteDiarioPage limitepage = new LimiteDiarioPage();
    private static final HomePage homepage = new HomePage();
    private static final TransferenciaTedPage transferencia = new TransferenciaTedPage();

    public void fluxoDeLimite(){
        loginPage.fazerLogin(LoginDataFactory.loginComUsuarioLimite());
        homepage.clicarNoBtnAreaFinanceira();
        transferencia.clicarNoBtnTransferencia();
        limitepage.clicarNoBtnLimiteDiario();
    }

    @Test
    @Feature("Limite diário")
    @Description("Validar modificação de limtie diario com sucesso")
    @Story("Alteração de límite diário")
    @Severity(SeverityLevel.NORMAL)
    public void validarAlteracaoDeLimiteDiario() {
        fluxoDeLimite();
        sleep(15000);
        limitepage.alterarLimite();

        Assert.assertTrue(limitepage.validaMensagemDeSucesso());
    }
}
