package org.dbcbank.test.transacoes.boleto;

import io.qameta.allure.*;
import org.dbcbank.data.dto.cliente.login.LoginDTO;
import org.dbcbank.data.dto.transacoes.boleto.BoletoDTO;
import org.dbcbank.page.cliente.login.LoginPage;
import org.dbcbank.page.transacoes.boleto.criacao.BoletoCriacaoPage;
import org.dbcbank.page.transacoes.boleto.pagamento.BoletoPagamentoPage;
import org.dbcbank.page.website.homepage.HomePage;
import org.dbcbank.rest.controller.TransacoesController;
import org.dbcbank.rest.controller.UsuarioController;
import org.dbcbank.rest.dto.usuario.LoginRequestModel;
import org.dbcbank.rest.dto.usuario.RegisterApiModel;
import org.dbcbank.test.BaseTeste;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;

public class PagamentoBoletoTest extends BaseTeste {
    private static final LoginPage loginPage = new LoginPage();
    private static final HomePage homePage = new HomePage();
    private static final BoletoCriacaoPage boletoCriacaoPage = new BoletoCriacaoPage();
    private static final BoletoPagamentoPage boletoPagamentoPage = new BoletoPagamentoPage();
    private RegisterApiModel usuario;
    private String usuarioToken;
    private String numeroCartaoCredito;

    @BeforeClass
    public void gerarMassaDeDados() {
        usuario = UsuarioController.cadastrarUsuario();
        usuarioToken = UsuarioController.logar(new LoginRequestModel(usuario));
        numeroCartaoCredito = UsuarioController.criarCartaoDeCredito(usuarioToken).getCardNumber();
        TransacoesController.realizarDeposito(
                usuario.getDocument(),
                UsuarioController.buscarUsuario(usuarioToken).getBankAccount().getAccNumber(),
                5000.00,
                usuarioToken
        );
    }

    @AfterClass
    public void limparMassaDeDados() {
        UsuarioController.excluirUsuario(usuarioToken);
    }

    public void logarUsuario() {
        loginPage.fazerLogin(new LoginDTO(usuario));
        sleep(3000);
        homePage.clicarNoBtnAreaFinanceira();
        homePage.validarUrlDaPagina("https://dbcbank-questers-front-vs3.onrender.com/admin/financas/transferencia");
    }

    @Test
    @Feature("Pagamento de Boleto")
    @Description("Validar pagamento de boleto com cartão de crédito com sucesso")
    @Story("Pagamento de boleto com cartão de crédito")
    @Severity(SeverityLevel.CRITICAL)
    public void testPagamentoDeBoletoComCartaoDeCreditoComSucesso() {

        logarUsuario();
        BoletoDTO dadosBoletoGerado = boletoCriacaoPage.gerarBoletoValido();
        boletoPagamentoPage.preencherCodigoDeBarras(dadosBoletoGerado.getCodigoBarras());
        boletoPagamentoPage.revisarDadosBoleto(dadosBoletoGerado.getValor());
        boletoPagamentoPage.escolherPagamentoComCartaoDeCredito(numeroCartaoCredito);
        boletoPagamentoPage.confirmarPagamento(usuario.getTransactionPwd());
        boletoPagamentoPage.verificarSucessoNoPagamentoDoBoleto(dadosBoletoGerado.getCodigoBarras());
    }

    @Test
    @Feature("Pagamento de Boleto")
    @Description("Validar pagamento de boleto com saldo em conta com sucesso")
    @Story("Pagamento de boleto com saldo em conta")
    @Severity(SeverityLevel.CRITICAL)
    public void testPagamentoDeBoletoComSaldoEmContaComSucesso() {

        logarUsuario();
        BoletoDTO dadosBoletoGerado = boletoCriacaoPage.gerarBoletoValido();
        boletoPagamentoPage.preencherCodigoDeBarras(dadosBoletoGerado.getCodigoBarras());
        boletoPagamentoPage.revisarDadosBoleto(dadosBoletoGerado.getValor());
        boletoPagamentoPage.escolherPagamentoComSaldoEmConta();
        boletoPagamentoPage.confirmarPagamento(usuario.getTransactionPwd());
        boletoPagamentoPage.verificarSucessoNoPagamentoDoBoleto(dadosBoletoGerado.getCodigoBarras());
    }
}
