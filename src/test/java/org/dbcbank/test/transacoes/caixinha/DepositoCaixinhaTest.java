package org.dbcbank.test.transacoes.caixinha;

import io.qameta.allure.*;
import org.dbcbank.data.dto.transacoes.caixinha.DinheiroCaixinhaDTO;
import org.dbcbank.data.factory.datafactory.transacoes.caixinha.DinheiroCaixinhaDataFactory;
import org.dbcbank.data.factory.datafactory.cliente.login.LoginDataFactory;
import org.dbcbank.page.transacoes.caixinha.DepositoCaixinhaPage;
import org.dbcbank.page.website.homepage.HomePage;
import org.dbcbank.page.cliente.login.LoginPage;
import org.dbcbank.test.BaseTeste;
import org.testng.annotations.Test;

public class DepositoCaixinhaTest extends BaseTeste {
    private final HomePage homePage = new HomePage();
    private final LoginPage loginPage = new LoginPage();
    private final DepositoCaixinhaPage depositoCaixinha = new DepositoCaixinhaPage();

    @Test
    @Feature("Deposito na Caixinha")
    @Description("Validar deposito na caixinha")
    @Story("Deposito na caixinha")
    @Severity(SeverityLevel.CRITICAL)
    public void validarDepositoNaCaixinhaComSucesso() {
        loginPage.fazerLogin(LoginDataFactory.loginDadosValidos());
        homePage.clicarNoBtnAreaFinanceira();

        depositoCaixinha.clicarBtnGuardarDinheiroNaCaixinha();
        DinheiroCaixinhaDTO deposito = DinheiroCaixinhaDataFactory.dinheiroCaixinha();
        depositoCaixinha.realizarDepositoNaCaixinha(deposito);
        depositoCaixinha.verificarMsgDeDepositoComSucesso("Dinheiro guardado com sucesso");


        homePage.clicarNoBtnLogout();
    }
}
