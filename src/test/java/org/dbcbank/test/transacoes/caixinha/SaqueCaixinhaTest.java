package org.dbcbank.test.transacoes.caixinha;

import io.qameta.allure.*;
import org.dbcbank.data.dto.transacoes.caixinha.DinheiroCaixinhaDTO;
import org.dbcbank.data.factory.datafactory.transacoes.caixinha.DinheiroCaixinhaDataFactory;
import org.dbcbank.data.factory.datafactory.cliente.login.LoginDataFactory;
import org.dbcbank.page.transacoes.caixinha.SaqueCaixinhaPage;
import org.dbcbank.page.website.homepage.HomePage;
import org.dbcbank.page.cliente.login.LoginPage;
import org.dbcbank.test.BaseTeste;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;

public class SaqueCaixinhaTest extends BaseTeste {
    private final HomePage homePage = new HomePage();
    private final LoginPage loginPage = new LoginPage();
    private final SaqueCaixinhaPage saqueCaixinhaPage = new SaqueCaixinhaPage();

    @Test
    @Feature("Saque na Caixinha")
    @Description("Validar saque na caixinha")
    @Story("Saque na caixinha")
    @Severity(SeverityLevel.CRITICAL)
    public void validarSaqueNaCaixinhaComSucesso() {
        loginPage.fazerLogin(LoginDataFactory.loginDadosValidos());
        homePage.clicarNoBtnAreaFinanceira();

        saqueCaixinhaPage.clicarBtnGuardarDinheiroNaCaixinha();
        sleep(3000);
        DinheiroCaixinhaDTO saque = DinheiroCaixinhaDataFactory.dinheiroCaixinha();
        saqueCaixinhaPage.realizarSaqueNaCaixinha(saque);

        saqueCaixinhaPage.verificarMsgDeSaqueComSucesso("Dinheiro resgatado com sucesso");

        homePage.clicarNoBtnLogout();
    }
}
