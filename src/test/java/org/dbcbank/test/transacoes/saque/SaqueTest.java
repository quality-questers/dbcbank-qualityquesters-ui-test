package org.dbcbank.test.transacoes.saque;

import io.qameta.allure.*;
import org.dbcbank.data.factory.datafactory.cliente.login.LoginDataFactory;
import org.dbcbank.page.website.homepage.HomePage;
import org.dbcbank.page.cliente.login.LoginPage;
import org.dbcbank.page.transacoes.saque.SaquePage;
import org.dbcbank.test.BaseTeste;
import org.testng.Assert;
import org.testng.annotations.Test;


public class SaqueTest extends BaseTeste {
    private static final HomePage homePage = new HomePage();
    private static final LoginPage loginPage = new LoginPage();
    private static final SaquePage saquePage = new SaquePage();

    public void fluxoDeSaque(){
        loginPage.fazerLogin(LoginDataFactory.loginUsuarioRemetente());
        homePage.clicarNoBtnAreaFinanceira();
        saquePage.clicarNoBtnLateralDeSaque();
        saquePage.clicarNoBtnSaque();
        saquePage.validarUrlDaPagina("https://dbcbank-questers-front-vs3.onrender.com/admin/financas/saque/sacar");
    }


    @Test
    @Feature("Saque")
    @Description("Validar saque em conta corrente com sucesso")
    @Story("Saque de dinheiro em conta corrente")
    @Severity(SeverityLevel.NORMAL)
    public void validarSaqueEmContaCorrenteComSucesso() {
        fluxoDeSaque();
        saquePage.realizarSaque();

        Assert.assertTrue(saquePage.validarMensagemDeSucesso());
    }

    @Test
    @Feature("Saque")
    @Description("Validar saque sem suficiente em conta")
    @Story("Saque de dinheiro sem saldo em conta")
    @Severity(SeverityLevel.NORMAL)
    public void validarSaqueEmContaCorrenteSemDinheiroNaConta() {
        fluxoDeSaque();
        saquePage.realizarSaqueAcimaDoValorEmConta();

        Assert.assertTrue(saquePage.validarMensagemDeFundosInsuficientes());
    }
}
