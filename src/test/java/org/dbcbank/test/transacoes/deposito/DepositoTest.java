package org.dbcbank.test.transacoes.deposito;

import io.qameta.allure.*;
import org.dbcbank.data.dto.transacoes.deposito.DepositoDTO;
import org.dbcbank.data.factory.datafactory.transacoes.deposito.DepositoDataFactory;
import org.dbcbank.data.factory.datafactory.cliente.login.LoginDataFactory;
import org.dbcbank.page.transacoes.deposito.DepositoPage;
import org.dbcbank.page.website.homepage.HomePage;
import org.dbcbank.page.cliente.login.LoginPage;
import org.dbcbank.test.BaseTeste;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;


public class DepositoTest extends BaseTeste {
    HomePage homePage = new HomePage();
    LoginPage loginPage = new LoginPage();
    DepositoPage depositoPage = new DepositoPage();

    @Test
    @Feature("Deposito")
    @Description("Validar deposito em conta corrente")
    @Story("Deposito em conta corrente")
    @Severity(SeverityLevel.CRITICAL)
    public void validarDepositoEmContaCorrente() {
        loginPage.fazerLogin(LoginDataFactory.loginDadosValidos());
        homePage.clicarNoBtnAreaFinanceira();
        depositoPage.clicarNoBtnDeposito();

        depositoPage.clicarNoBotaoDepositar();
        depositoPage.validarUrlDaPagina("https://dbcbank-questers-front-vs3.onrender.com/admin/financas/deposito/add");

        sleep(5000);
        DepositoDTO deposito = DepositoDataFactory.realizarDeposito();
        depositoPage.realizarDeposito(deposito);

        depositoPage.clicarNoBotaoDeConfirmarDeposito();
        depositoPage.voltarPagina();

        homePage.clicarNoBtnLogout();
    }
}
