package org.dbcbank.page.website.paginainicial;

import com.codeborne.selenide.SelenideElement;
import org.dbcbank.page.BasePage;

import static com.codeborne.selenide.Selenide.$;

public class InicialPage extends BasePage {
    private static final SelenideElement BTN_CADASTRAR = $("#root > div.flex.flex-col.w-full > div.w-full.relative.flex.items-center > div > div > button");
    private static final SelenideElement BTN_VOLTAR = $("#root > div.flex.flex-col.w-full > div.w-full.h-\\[96px\\].bg-primary.flex.items-center > div > div");
    private static final SelenideElement TXT_DBC_BANK = $(".text-lg");

    public void validarTextoDaHomePage(String text){
        verificarTexto(TXT_DBC_BANK, text);
    }

    public void clicarNoBtnRegistrar() {
        clicar(BTN_CADASTRAR);
    }

    public void clicarNoBtnVoltar() {
        clicar(BTN_VOLTAR);
    }
}
