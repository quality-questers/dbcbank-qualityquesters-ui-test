package org.dbcbank.page.website.homepage;

import com.codeborne.selenide.SelenideElement;
import org.dbcbank.page.BasePage;

import static com.codeborne.selenide.Selenide.$;

public class HomePage extends BasePage {
    private static final SelenideElement BTN_LOGOUT = $("#root > div.flex-col.justify-between.flex.bg-secondary.min-h-full > div > div.w-full.bg-primary.h-\\[96px\\].fixed.z-50.px-10.flex.items-center.justify-between.pointer-events-auto > div.flex.items-center.gap-3 > svg");
    private static final SelenideElement BTN_PERFIL = $("[data-testid='perfil']");
    private static final SelenideElement BTN_AREA_FINANCEIRA =  $("[data-testid='finance']");
    private static final SelenideElement BTN_CARTOES = $("[data-testid='cards']");

    public void clicarNoBtnLogout() {
        clicar(BTN_LOGOUT);
    }

    public void clicarNoBtnAreaFinanceira() {
        clicar(BTN_AREA_FINANCEIRA);
    }

    public void clicarNoBtnPerfil() {
        clicar(BTN_PERFIL);
    }

    public void clicarNoBtnCartoes() {
        clicar(BTN_CARTOES);
    }
}
