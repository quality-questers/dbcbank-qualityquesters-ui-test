package org.dbcbank.page.website.cartoes;

import org.dbcbank.page.BasePage;
import org.dbcbank.selectors.cartoes.CartoesSelectors;

public class CartoesPage extends BasePage {
    private static final String MENSAGEM_DE_SUCESSO = "Solicitação feita com sucesso!";
    private static final String CARTAO_REMETENTE = "7558 5718 2905 0485";

    // Click
    public void clicarBtnSolicitarCartao(){
        clicar(CartoesSelectors.BTN_SOLICITAR_CARTAO);
    }

    public void clicarBtnCartaoDeCredito(){
        clicar(CartoesSelectors.BTN_CARTAO_DE_CREDITO);
    }

    //Métodos
    public boolean validarMensagemDeSucesso(){
        return verificarTextoAssertion(CartoesSelectors.MENSAGEM_DE_SUCESSO_SELETOR, MENSAGEM_DE_SUCESSO);
    }

    public boolean validarNumeroDoCartao(){
        return verificarTextoAssertion(CartoesSelectors.NUMERO_CARTAO, CARTAO_REMETENTE);
    }
}
