package org.dbcbank.page.transacoes.saque;

import org.dbcbank.data.dto.transacoes.saque.SaqueDTO;
import org.dbcbank.data.factory.datafactory.transacoes.saque.SaqueDataFactory;
import org.dbcbank.page.BasePage;
import org.dbcbank.selectors.transacoes.saque.SaqueSelectors;
import org.dbcbank.util.MetodosAuxiliares;

public class SaquePage extends BasePage {
    public static final String MENSAGEM_DE_SUCESSO = "Saque realizado com sucesso";
    public static final String MENSAGEM_FUNDOS_INSUFICIENTES = "Fundos insuficientes.";

    //Btn
    public void clicarNoBtnLateralDeSaque() {
        clicar(SaqueSelectors.BTN_LATERAL_DE_SAQUE);
    }
    public void clicarNoBtnSaque(){
        clicar(SaqueSelectors.BTN_SAQUE);
    }
    public void clicarNoBtnSubmit(){
        clicar(SaqueSelectors.BTN_SUBMIT_SAQUE);
    }
    public void clicarBtnSacar(){
        clicar(SaqueSelectors.BTN_SACAR);
    }

    //Métodos
    public boolean validarMensagemDeSucesso(){
       return verificarTextoAssertion(SaqueSelectors.MENSAGEM_DE_SUCESSO_ELEMENT ,MENSAGEM_DE_SUCESSO);
    }

    public boolean validarMensagemDeFundosInsuficientes(){
        return verificarTextoAssertion(SaqueSelectors.MENSAGEM_DE_FUNDOS_INSUFICIENTES ,MENSAGEM_FUNDOS_INSUFICIENTES);
    }

    public void realizarSaque(){
        SaqueDTO saque = SaqueDataFactory.realizarSaqueValido();

        String valorSaque = saque.getAmount().toString();
        preencherInputNumero(SaqueSelectors.INPUT_VALOR_SAQUE, valorSaque);
        clicarBtnSacar();
        MetodosAuxiliares.preencherSenhaDeTransacao("1995");
        clicarNoBtnSubmit();
    }

    public void realizarSaqueAcimaDoValorEmConta(){
        SaqueDTO saque = SaqueDataFactory.realizarSaqueAcimaDoValorEmConta();

        String valorSaque = saque.getAmount().toString();

        preencherInputNumero(SaqueSelectors.INPUT_VALOR_SAQUE, valorSaque);
        clicarBtnSacar();
        MetodosAuxiliares.preencherSenhaDeTransacao("1995");
        clicarNoBtnSubmit();
    }
}