package org.dbcbank.page.transacoes.deposito;

import org.dbcbank.data.dto.transacoes.deposito.DepositoDTO;
import org.dbcbank.page.BasePage;
import org.dbcbank.selectors.transacoes.deposito.DepositoSelectors;

public class DepositoPage extends BasePage {
    public void clicarNoBtnDeposito() {
        clicar(DepositoSelectors.BTN_DEPOSITO);
    }

    public void clicarNoBotaoDepositar(){
        clicar(DepositoSelectors.BTN_DEPOSITAR);
    }

    public void clicarNoBotaoDeConfirmarDeposito(){
        clicar(DepositoSelectors.BTN_REALIZAR_DEPOSITO);
    }

    public void realizarDeposito(DepositoDTO deposito){
        preencherInputText(DepositoSelectors.NR_CPF_DESTINATARIO, deposito.getCpf());
        preencherInputText(DepositoSelectors.NR_CONTA_DESTINATARIO, deposito.getAccNumber());
        String valorDeposito = deposito.getAmount().toString();
        preencherInputNumero(DepositoSelectors.INPUT_VALOR, valorDeposito);
    }

    public void voltarPagina(){
        clicar(DepositoSelectors.BTN_VOLTAR);
    }
}
