package org.dbcbank.page.transacoes.boleto.criacao;

import org.dbcbank.data.dto.transacoes.boleto.BoletoDTO;
import org.dbcbank.page.BasePage;

import static org.dbcbank.selectors.transacoes.boleto.BoletoSelectors.*;

public class BoletoCriacaoPage extends BasePage {

    public BoletoDTO gerarBoletoValido() {
        clicarBtnBoletos();
        validarUrlDaPaginaInicialBoleto();

        clicarBtnGerarBoletoPrimeiraEtapa();
        validarUrlDaPaginaCriacaoDeBoleto();

        clicarBtnGerarBoletoSegundaEtapa();
        validarMensagemBoletoCriadoComSucesso();
        String valorBoletoGerado = obterValorBoletoGerado();
        String codigoDeBarras = obterCodigoDeBarrasBoletoGerado();
        clicarBtnVoltarPaginaAnteriorPosGeracaoBoleto();
        validarUrlDaPaginaInicialBoleto();

        return new BoletoDTO(codigoDeBarras, valorBoletoGerado);
    }

    private void clicarBtnBoletos() {
        clicar(BTN_BOLETO);
    }

    private void clicarBtnGerarBoletoPrimeiraEtapa() {
        clicar(BTN_GERAR_BOLETO_PRIMEIRA_ETAPA);
    }

    private void clicarBtnGerarBoletoSegundaEtapa() {
        clicar(BTN_GERAR_BOLETO_SEGUNDA_ETAPA);
    }

    private void clicarBtnVoltarPaginaAnteriorPosGeracaoBoleto() {
        clicar(BTN_VOLTAR_PAGINA_ANTERIOR_POS_CRIACAO_BOLETO);
    }

    private String obterValorBoletoGerado() {
        return obterValorElemento(MSG_VALOR_BOLETO);
    }

    private String obterCodigoDeBarrasBoletoGerado() {
        return obterValorElemento(MSG_NUMERO_CODIGO_DE_BARRAS_CRIADO);
    }

    private void validarMensagemBoletoCriadoComSucesso() {
        verificarTexto(MSG_BOLETO_CRIADO_COM_SUCESSO, "Boleto Gerado");
    }

    private void validarUrlDaPaginaInicialBoleto() {
        super.validarUrlDaPagina("https://dbcbank-questers-front-vs3.onrender.com/admin/financas/boleto");
    }

    private void validarUrlDaPaginaCriacaoDeBoleto() {
        super.validarUrlDaPagina("https://dbcbank-questers-front-vs3.onrender.com/admin/financas/boleto/gerar");
    }
}
