package org.dbcbank.page.transacoes.boleto.pagamento;

import com.codeborne.selenide.SelenideElement;
import org.dbcbank.data.factory.datafactory.transacoes.boleto.BoletoDataFactory;
import org.dbcbank.page.BasePage;
import org.dbcbank.util.MetodosAuxiliares;

import static com.codeborne.selenide.Selenide.$;
import static org.dbcbank.selectors.transacoes.boleto.BoletoSelectors.*;

public class BoletoPagamentoPage extends BasePage {

    public void preencherCodigoDeBarras(String codigoDeBarras) {
        clicarBtnPagarBoleto();
        validarUrlDaPaginaPagamentoBoleto();
        validarMensagemPaginaCodigoDeBarras();
        preencherInputCodigoBarras(codigoDeBarras);
        clicarBtnContinuarPagamentoPosCodigoBarras();
        validarMensagemPaginaConfirmacaoDadosBoleto();
    }

    public void revisarDadosBoleto(String valorBoletoGerado) {
        validarValorBoletoPaginaConfirmacaoDadosBoleto(valorBoletoGerado);
        clicarBtnContinuarPagamentoPosRevisaoDadosBoleto();
        validarMensagemPaginaEscolherFormaDePagamento();
    }

    public void escolherPagamentoComCartaoDeCredito(String numeroCartaoCredito) {
        ecolherPagamentoNoCredito(numeroCartaoCredito);
        preencherInputDescricaoPagamento();
        clicarBtnContinuarPagamentoPosEscolhaFormaPagamento();
        validarMensagemPaginaSenhaTransacao();
    }

    public void escolherPagamentoComSaldoEmConta() {
        preencherInputDescricaoPagamento();
        clicarBtnContinuarPagamentoPosEscolhaFormaPagamento();
        validarMensagemPaginaSenhaTransacao();
    }


    public void confirmarPagamento(String senhaDeTransacao) {
        preencherSenhaTransacao(senhaDeTransacao);
        clicarBtnContinuarPagamentoPosInformarSenhaPagamento();
        validarMensagemBoletoPagoComSucesso();
    }

    public void verificarSucessoNoPagamentoDoBoleto(String codigoDeBarras) {
        clicarBtnBoletosPosPagamento();
        preencherCodigoDeBarras(codigoDeBarras);
        validarStatusPagamentoBoleto();
    }


    private void clicarBtnBoletosPosPagamento() {
        clicar(BTN_BOLETO_POS_PAGAMENTO);
    }

    private void clicarBtnPagarBoleto() {
        clicar(BTN_PAGAR_BOLETO);
    }

    private void clicarBtnContinuarPagamentoPosCodigoBarras() {
        clicar(BTN_CONTINUAR_PAGAMENTO_POS_CODIGO_BARRAS);
    }

    private void clicarBtnContinuarPagamentoPosRevisaoDadosBoleto() {
        clicar(BTN_CONTINUAR_PAGAMENTO_POS_REVISAO_DADOS_BOLETO);
    }

    private void clicarBtnContinuarPagamentoPosEscolhaFormaPagamento() {
        clicar(BTN_CONTINUAR_PAGAMENTO_POS_ESCOLHA_FORMA_PAGAMENTO);
    }

    private void clicarBtnContinuarPagamentoPosInformarSenhaPagamento() {
        clicar(BTN_CONTINUAR_PAGAMENTO_POS_INFORMAR_SENHA_PAGAMENTO);
    }

    private void ecolherPagamentoNoCredito(String numeroCartaoCredito) {
        SelenideElement elemento = $("[value=" + "'" + numeroCartaoCredito + "']");
        clicar(elemento);
    }

    private void preencherInputCodigoBarras(String valorCodigoBarras) {
        preencherInputText(INPUT_CODIGO_BARRAS, valorCodigoBarras);
    }

    private void preencherInputDescricaoPagamento() {
        preencherInputText(INPUT_DESCRICAO_PAGAMENTO, BoletoDataFactory.gerarTextoAleatorioCampoDecricao());
    }

    private void preencherSenhaTransacao(String senhaTransacao) {
        MetodosAuxiliares.preencherSenhaDeTransacao(senhaTransacao);
    }

    private void validarMensagemPaginaCodigoDeBarras() {
        verificarTexto(MSG_PAGINA_CODIGO_BARRAS, "Código de barras");
    }

    private void validarMensagemPaginaConfirmacaoDadosBoleto() {
        verificarTexto(MSG_PAGINA_REVISAR_INFORMACOES_BOLETO, "Revisar informações");
    }

    private void validarMensagemPaginaSenhaTransacao() {
        verificarTexto(MSG_PAGINA_SENHA_TRANSACAO, "Confirmar transferência");
    }

    private void validarMensagemPaginaEscolherFormaDePagamento() {
        verificarTexto(MSG_ESCOLHER_FORMA_PAGAMENTO, "Escolher forma de pagamento");
    }

    private void validarMensagemBoletoPagoComSucesso() {
        verificarTexto(MSG_BOLETO_PAGO_COM_SUCESSO, "Boleto pago com sucesso");
    }

    private void validarValorBoletoPaginaConfirmacaoDadosBoleto(String valorBoleto) {
        verificarTexto(MSG_PAGINAR_REVISAR_INFORMACOES_BOLETO_CONFIRMAR_VALOR_BOLETO, valorBoleto);
    }

    private void validarStatusPagamentoBoleto() {
        verificarTexto(MSG_PAGINAR_REVISAR_INFORMACOES_BOLETO_STATUS_BOLETO_PAGO, "Pago");
    }

    private void validarUrlDaPaginaPagamentoBoleto() {
        super.validarUrlDaPagina("https://dbcbank-questers-front-vs3.onrender.com/admin/financas/boleto/pagar");
    }
}
