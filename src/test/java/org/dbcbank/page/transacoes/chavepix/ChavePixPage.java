package org.dbcbank.page.transacoes.chavepix;

import org.dbcbank.data.factory.datafactory.transacoes.chavepix.ChavePixDataFactory;
import org.dbcbank.enums.TipoChavePix;
import org.dbcbank.page.BasePage;
import org.dbcbank.selectors.transacoes.chavepix.ChavePixSelectors;

import static com.codeborne.selenide.Selenide.$;

public class ChavePixPage extends BasePage {
    public static String tipoChavePixCriada = "";

    public void clicarNoBtnGerenciaPix() {
        clicar(ChavePixSelectors.BTN_CHAVE_PIX);
    }

    private void clicarNoBtnCriarPix() {
        clicar(ChavePixSelectors.BTN_CRIAR_CHAVE);
    }

    private void clicarNoBtnConfirmarCriacaoChavePix() {
        clicar(ChavePixSelectors.BTN_CONFIRMAR_CRIACAO_CHAVE);
    }

    private void clicarNoSelectChavePix() {
        clicar(ChavePixSelectors.SELECT_TIPO_CHAVE);
    }

    private void selecionarTipoChavePixAleatoriamente() {
        TipoChavePix tipoChavePix = ChavePixDataFactory.gerarOpcoesChavePix();

        String descricaoChavePix = tipoChavePix.getDescricao();
        int codigoChavePix = tipoChavePix.getCodigo();

        String elemento = "#radix-\\:rb\\: > div > div:nth-child(" + codigoChavePix + ")";
        clicar($(elemento));

        tipoChavePixCriada = descricaoChavePix;
    }

    public void criarChavePix(){
        clicarNoBtnCriarPix();
        clicarNoSelectChavePix();
        selecionarTipoChavePixAleatoriamente();
        clicarNoBtnConfirmarCriacaoChavePix();
    }

    public void verificarTipoChaveCriada(){
        verificarTexto(ChavePixSelectors.TIPO_CHAVE_CRIADA, tipoChavePixCriada);
    }

    public void verificarKeyPixCriada(String keyChavePix){
        verificarTexto(ChavePixSelectors.KEY_CHAVE_CRIADA, keyChavePix);
    }
}
