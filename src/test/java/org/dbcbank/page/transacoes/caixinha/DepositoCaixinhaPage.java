package org.dbcbank.page.transacoes.caixinha;

import org.dbcbank.data.dto.transacoes.caixinha.DinheiroCaixinhaDTO;
import org.dbcbank.page.BasePage;
import org.dbcbank.selectors.transacoes.caixinha.DepositoCaixinhaSelectors;

public class DepositoCaixinhaPage extends BasePage {
    public void clicarBtnGuardarDinheiroNaCaixinha(){
        clicar(DepositoCaixinhaSelectors.BTN_GUARDAR_DINHEIRO_CAIXINHA);
    }

    public void realizarDepositoNaCaixinha(DinheiroCaixinhaDTO deposito){
        clicar(DepositoCaixinhaSelectors.BTN_GUARDAR_DINHEIRO);
        String valorDeposito = deposito.getAmount().toString();
        preencherInputNumero(DepositoCaixinhaSelectors.INPUT_GUARDAR_DINHEIRO, valorDeposito);
        clicar(DepositoCaixinhaSelectors.BTN_GUARDAR);
    }

    public void verificarMsgDeDepositoComSucesso(String text){
        verificarTexto(DepositoCaixinhaSelectors.MSG_DEPOSITO_CAIXINHO_SUCESSO, text);
    }
}
