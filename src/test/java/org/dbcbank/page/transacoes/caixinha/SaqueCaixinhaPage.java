package org.dbcbank.page.transacoes.caixinha;

import org.dbcbank.data.dto.transacoes.caixinha.DinheiroCaixinhaDTO;
import org.dbcbank.page.BasePage;
import org.dbcbank.selectors.transacoes.caixinha.SaqueCaixinhaSelectors;

public class SaqueCaixinhaPage extends BasePage {
    public void clicarBtnGuardarDinheiroNaCaixinha(){
        clicar(SaqueCaixinhaSelectors.BTN_GUARDAR_DINHEIRO_CAIXINHA);
    }

    public void realizarSaqueNaCaixinha(DinheiroCaixinhaDTO saque){
        clicar(SaqueCaixinhaSelectors.BTN_SAQUE_DINHEIRO);
        String valorSaque = saque.getAmount().toString();
        preencherInputNumero(SaqueCaixinhaSelectors.INPUT_SACAR_DINHEIRO, valorSaque);
        clicar(SaqueCaixinhaSelectors.BTN_SACAR);
    }

    public void verificarMsgDeSaqueComSucesso(String text){
        verificarTexto(SaqueCaixinhaSelectors.MSG_SAQUE_CAIXINHO_SUCESSO, text);
    }
}
