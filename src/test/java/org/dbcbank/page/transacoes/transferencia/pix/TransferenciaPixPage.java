package org.dbcbank.page.transacoes.transferencia.pix;

import org.dbcbank.data.dto.transacoes.transferencia.pix.TransferenciaPixDTO;
import org.dbcbank.data.factory.datafactory.transacoes.transferencia.pix.TransferenciaPixDataFactory;
import org.dbcbank.page.BasePage;
import org.dbcbank.selectors.transacoes.transferencia.pix.TransferenciaPixSelectors;
import org.dbcbank.util.MetodosAuxiliares;

public class TransferenciaPixPage extends BasePage {
    public void clicarNoBtnTransferencia(){
        clicar(TransferenciaPixSelectors.BTN_LATERAL_TRANSFERENCIA);
    }

    private void clicarNoBtnTransferenciaPix() {
        clicar(TransferenciaPixSelectors.BTN_TRANSFERENCIA);
    }

    private void clicarNoBtnSubmitTransferencia() {
        clicar(TransferenciaPixSelectors.BTN_SUBMIT_TRANSFERENCIA);
    }

    private void clicarNoBtnRevisaoDadosTransferencia() {
        clicar(TransferenciaPixSelectors.BTN_CONFIRMAR_REVISAO_DADOS_TRANSFERENCIA);
    }

    private void preencherSenhaDeTransacao(String senhaTransacao) {
        MetodosAuxiliares.preencherSenhaDeTransacao(senhaTransacao);
    }

    public void clicarNoBtnConfirmarTransferenciaPix() {
        clicar(TransferenciaPixSelectors.BTN_SUBMIT_CONFIRMAR);
    }

    public void preencherDadosTransferencia() {
        TransferenciaPixDTO transferencia = TransferenciaPixDataFactory.gerarTransferenciaValida();

        clicarNoBtnTransferenciaPix();
        preencherInputNumero(TransferenciaPixSelectors.INPUT_VALOR_TRANSFERENCIA, String.valueOf(transferencia.getAmount()));
        preencherInputText(TransferenciaPixSelectors.INPUT_ACCOUNT, transferencia.getDestinationKey());

        clicarNoBtnSubmitTransferencia();
    }

    public void confirmarDadosTransferencia(String senhaTransacao) {
        clicarNoBtnRevisaoDadosTransferencia();
        preencherSenhaDeTransacao(senhaTransacao);
        clicarNoBtnConfirmarTransferenciaPix();
    }

    public void validarMensagemDeSucesso(String mensagem){
        verificarTexto(TransferenciaPixSelectors.TITULO_TRANSFERENCIA_SUCESSO, mensagem);
    }
}
