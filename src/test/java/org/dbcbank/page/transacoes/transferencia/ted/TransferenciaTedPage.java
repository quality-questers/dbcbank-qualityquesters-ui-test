package org.dbcbank.page.transacoes.transferencia.ted;

import org.dbcbank.data.dto.transacoes.transferencia.ted.TransferenciaTedDTO;
import org.dbcbank.data.factory.datafactory.transacoes.transferencia.ted.TransferenciaTedDataFactory;
import org.dbcbank.page.BasePage;
import org.dbcbank.selectors.transacoes.transferencia.ted.TransferenciaTedSelectors;
import org.dbcbank.util.MetodosAuxiliares;

public class TransferenciaTedPage extends BasePage {
    //Messages
    private static final String MENSAGEM_DE_SUCESSO = "Transferência realizada com sucesso";
    private static final String MENSAGEM_DE_ERRO_VALIDACAO = "Nenhuma conta associada à este número foi encontrada. Verifique se o número da conta foi digitado corretamente no formato 'xxxxx-x'.";
    private static final String SENHA_TRANSACAO_REMENTENTE = "1995";

    //Clicks
    public void clicarNoBtnTransferenciaTed(){
        clicar(TransferenciaTedSelectors.BTN_TRANSFERENCIA);
    }

    public void clicarNoBtnTransferencia(){
        clicar(TransferenciaTedSelectors.BTN_LATERAL_TRANSFERENCIA);
    }

    public void clicarNoBtnSubmitTransferencia(){
        clicar(TransferenciaTedSelectors.BTN_SUBMIT_TRANSFERENCIA);
    }

    public void clicarNoBtnConfirmarTransferenciaTed(){
        clicar(TransferenciaTedSelectors.BTN_SUBMIT_CONFIRMAR);
    }

    // Métodos
    public boolean validarMensagemDeSucesso(){
        return verificarTextoAssertion(TransferenciaTedSelectors.MENSAGEM_DE_SUCESSO_TRANSFERENCIA_TED ,MENSAGEM_DE_SUCESSO);
    }

    public boolean validarMensagemDeErroDeValidacao(){
        return verificarTextoAssertion(TransferenciaTedSelectors.MENSAGEM_ERRO_DE_VALIDACAO , MENSAGEM_DE_ERRO_VALIDACAO);
    }


    public void realizarTransferencia(){
        TransferenciaTedDTO transferencia = TransferenciaTedDataFactory.transferenciaValida();
        String[] partesDaConta = transferencia.getDestinationAccNumber().split("-");
        String numeroConta = partesDaConta[0];
        String digitoVerificador = partesDaConta[1];

        preencherInputNumero(TransferenciaTedSelectors.INPUT_VALOR_TRANSFERENCIA, String.valueOf(transferencia.getAmount()));
        preencherInputNumero(TransferenciaTedSelectors.INPUT_BRANCH_ID, String.valueOf(transferencia.getDestinationBranchId()));
        preencherInputNumero(TransferenciaTedSelectors.INPUT_ACCOUNT, numeroConta);
        preencherInputNumero(TransferenciaTedSelectors.INPUT_DIGITO, digitoVerificador);
        preencherInputNumero(TransferenciaTedSelectors.INPUT_DESCRIPTION, transferencia.getTransactionDescription());
        clicarNoBtnSubmitTransferencia();
        clicarNoBtnSubmitTransferencia();
        preencherSenhaDeTransacao();
        clicarNoBtnConfirmarTransferenciaTed();
    }

    public void realizarTransferenciaComContaInvalida(){
        TransferenciaTedDTO transferencia = TransferenciaTedDataFactory.transferenciaParaContaInvalida();
        String[] partesDaConta = transferencia.getDestinationAccNumber().split("-");
        String numeroConta = partesDaConta[0];
        String digitoVerificador = partesDaConta[1];

        preencherInputNumero(TransferenciaTedSelectors.INPUT_VALOR_TRANSFERENCIA, String.valueOf(transferencia.getAmount()));
        preencherInputNumero(TransferenciaTedSelectors.INPUT_BRANCH_ID, String.valueOf(transferencia.getDestinationBranchId()));
        preencherInputNumero(TransferenciaTedSelectors.INPUT_ACCOUNT, numeroConta);
        preencherInputNumero(TransferenciaTedSelectors.INPUT_DIGITO, digitoVerificador);
        preencherInputNumero(TransferenciaTedSelectors.INPUT_DESCRIPTION, transferencia.getTransactionDescription());
        clicarNoBtnSubmitTransferencia();
    }

    public void preencherSenhaDeTransacao(){
        MetodosAuxiliares.preencherSenhaDeTransacao(SENHA_TRANSACAO_REMENTENTE);
    }
}
