package org.dbcbank.page.transacoes.limitediario;

import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import org.dbcbank.page.BasePage;
import org.openqa.selenium.interactions.Actions;

import static com.codeborne.selenide.Condition.clickable;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public class LimiteDiarioPage extends BasePage {

    //Seletores
    private static final SelenideElement LIMITE_DIARIO_AREA = $(byText("Ajustar limite de transferência"));
    private static final SelenideElement BARRA_LIMITE = $("#root > div.flex-col.justify-between.flex.bg-secondary.min-h-full > div > div.container.pt-8.mt-\\[96px\\] > div > div.items-start.justify-between.flex.gap-4.relative > div.flex.flex-col.w-full > div > div > div > span > span:nth-child(2) > span");
    private static final SelenideElement BTN_SUBMIT = $(byText("Ajustar limite"));
    private static final SelenideElement MENSAGEM_SUCESSO_ELEMENT = $(byText("Limite ajustado com sucesso"));
    private static final SelenideElement VALOR_ATUAL = $("#root > div.flex-col.justify-between.flex.bg-secondary.min-h-full > div > div.container.pt-8.mt-\\[96px\\] > div > div.items-start.justify-between.flex.gap-4.relative > div.flex.flex-col.w-full > div > div > div > div.items-end.flex.gap-2 > span.text-slate-900.text-3xl.font-bold");
    private static final String MENSAGEM_SUCESSO = "Limite ajustado com sucesso";

    //Clicks
    public void clicarNoBtnLimiteDiario() {
        clicar(LIMITE_DIARIO_AREA);
    }

    public void clicarNoBtnSubmit() {
        clicar(BTN_SUBMIT);
    }

    //Operações
    public void alterarLimite() {
        Actions actions = new Actions(WebDriverRunner.getWebDriver());
        String valorAtual = VALOR_ATUAL.shouldHave(visible).getText();

        if (valorAtual.equals("10.000,00")) {
            actions.clickAndHold(BARRA_LIMITE.shouldBe(visible, clickable)).moveByOffset(-150, 0).release().perform();
        } else {
            actions.clickAndHold(BARRA_LIMITE.shouldBe(visible, clickable)).moveByOffset(50, 0).release().perform();
        }

        clicarNoBtnSubmit();
    }

    public boolean validaMensagemDeSucesso() {
        return verificarTextoAssertion(MENSAGEM_SUCESSO_ELEMENT, MENSAGEM_SUCESSO);
    }


}
