package org.dbcbank.page.cliente.register;

import org.dbcbank.data.dto.cliente.registro.RegisterDTO;
import org.dbcbank.page.BasePage;
import org.dbcbank.selectors.cliente.register.RegisterSelectors;

public class RegisterPage extends BasePage {
    public void voltarPaginaDeCadastro() {
        clicar(RegisterSelectors.BTN_VOLTAR);
    }

    public void btnFecharModalAlert() {
        clicar(RegisterSelectors.BTN_FECHAR_MODAL_ALERT);
    }

    public void verificarMensagemDeCadastroInvalido(String mensagemEsperada) {
        verificarTexto(RegisterSelectors.MSG_CADASTRO_INVALIDO, mensagemEsperada);
    }

    public void verificarMensagemDeCpfInvalido(String mensagemEsperada) {
        verificarTexto(RegisterSelectors.MSG_CPF_INVALIDO, mensagemEsperada);
    }

    public void irParaLoginAposCadastro() {
        clicar(RegisterSelectors.BTN_IR_PARA_LOGIN);
    }

    public void verificarUrlDaPagina(String expectedUrl) {
        validarUrlDaPagina(expectedUrl);
    }

    public void verificarMensagemNaPaginaDeCadastro() {
        verificarTexto(RegisterSelectors.MSG_TELA_CADASTRO, "Abra sua conta");
    }

    public void fazerCadastro(RegisterDTO registerDTO) {
        preencherInputText(RegisterSelectors.CAMPO_CPF, registerDTO.getCpf());
        preencherInputText(RegisterSelectors.CAMPO_CELULAR, registerDTO.getCelular());
        preencherInputText(RegisterSelectors.CAMPO_EMAIL, registerDTO.getEmail());
        preencherInputText(RegisterSelectors.CAMPO_CONFIRMAR_EMAIL, registerDTO.getConfirmarEmail());
        preencherInputText(RegisterSelectors.CAMPO_NOME_COMPLETO, registerDTO.getNomeCompleto());
        preencherInputText(RegisterSelectors.CAMPO_DATA_NASCIMENTO, registerDTO.getBirthdate());
        clicar(RegisterSelectors.BTN_PROXIMO);

        preencherInputText(RegisterSelectors.CAMPO_SENHA, registerDTO.getPassword());
        preencherInputText(RegisterSelectors.CAMPO_CONFIRMAR_SENHA, registerDTO.getPasswordConfirmacao());
        preencherInputText(RegisterSelectors.CAMPO_SENHA_CARTAO, registerDTO.getSenhaTransacao());
        preencherInputText(RegisterSelectors.CAMPO_CONFIRMAR_SENHA_CARTAO, registerDTO.getConfirmarSenhaTransacao());
        clicar(RegisterSelectors.CHECKBOX_PRIVACIDADE);
        clicar(RegisterSelectors.BTN_CRIAR_CONTA);
    }

    public void fazerCadastroInformandoCpfInvalido(RegisterDTO registerDTO) {
        preencherInputText(RegisterSelectors.CAMPO_CPF, registerDTO.getCpf());
        preencherInputText(RegisterSelectors.CAMPO_CELULAR, registerDTO.getCelular());
        preencherInputText(RegisterSelectors.CAMPO_EMAIL, registerDTO.getEmail());
        preencherInputText(RegisterSelectors.CAMPO_CONFIRMAR_EMAIL, registerDTO.getConfirmarEmail());
        preencherInputText(RegisterSelectors.CAMPO_NOME_COMPLETO, registerDTO.getNomeCompleto());
    }
}
