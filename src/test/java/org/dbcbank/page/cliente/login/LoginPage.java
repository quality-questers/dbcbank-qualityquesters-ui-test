package org.dbcbank.page.cliente.login;

import org.dbcbank.data.dto.cliente.login.LoginDTO;
import org.dbcbank.page.BasePage;
import org.dbcbank.selectors.cliente.login.LoginSelectors;

public class LoginPage extends BasePage {
    public void verificarMensagemDeLoginInvalido(String mensagemEsperada){
        verificarTexto(LoginSelectors.MSG_MENSAGEM_LOGIN_INVALIDO, mensagemEsperada);
    }

    public void fazerLogin(LoginDTO usuario) {
        preencherInputText(LoginSelectors.CAMPO_CPF, usuario.getCpf());
        preencherInputText(LoginSelectors.CAMPO_SENHA, usuario.getSenha());
        clicar(LoginSelectors.BTN_ACESSAR);
    }
}
