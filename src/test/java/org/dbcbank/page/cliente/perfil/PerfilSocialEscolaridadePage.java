package org.dbcbank.page.cliente.perfil;

import com.codeborne.selenide.SelenideElement;
import org.dbcbank.data.dto.cliente.perfil.PerfilSocialEscolaridadeDTO;
import org.dbcbank.page.BasePage;
import org.dbcbank.selectors.cliente.perfil.PerfilSocialEscolaridadeSelectors;

import static com.codeborne.selenide.Selenide.$;

public class PerfilSocialEscolaridadePage extends BasePage{

    public void clicarBtnSocialEscolaridade(){
        clicarNoMenuLateral(PerfilSocialEscolaridadeSelectors.BTN_SOCIAL_ESCOLARIDADE);
    }

    public void limparCampoProfissao(){
        limparInput(PerfilSocialEscolaridadeSelectors.BTN_PROFISSAO);
    }

    public void alterarSocialEscolaridade(PerfilSocialEscolaridadeDTO perfil) {
        selecionarDadosComButtonValue(perfil.getGenero());
        selecionarDadosComValue(perfil.getDeficiencia());

        // Verifica se a deficiência é "sim" e preenche a descrição se for o caso
        if ("sim".equals(perfil.getDeficiencia())) {
            preencherInputText(PerfilSocialEscolaridadeSelectors.INPUT_DESCRICAO, perfil.getDescricao());
        }

        selecionarDadosComButtonValue(perfil.getAutodeclaracaoRacial());
        selecionarDadosComButtonValue(String.valueOf(perfil.getDeficiencia()));
        selecionarDadosComButtonValue(perfil.getRendaMensal());
        selecionarInput(PerfilSocialEscolaridadeSelectors.BTN_GRAU_ESCOLARIDADE, perfil.getGrauDeEscolaridade());
        selecionarInput(PerfilSocialEscolaridadeSelectors.BTN_GRAU_ESCOLARIDADE, perfil.getGrauDeEscolaridade());
        selecionarInput(PerfilSocialEscolaridadeSelectors.BTN_REGIME_DE_TRABALHO, perfil.getRegimeDeTrabalho());
        limparCampoProfissao();
        preencherInputText(PerfilSocialEscolaridadeSelectors.BTN_PROFISSAO, perfil.getProfissao());
        selecionarInput(PerfilSocialEscolaridadeSelectors.BTN_TEMPO_DE_PROFISSAO, perfil.getTempoDeProfissao());
    }

    public void clicarBtnSalvar(){
        clicar(PerfilSocialEscolaridadeSelectors.BTN_SALVAR);
    }

    private static void selecionarDadosComButtonValue(String elementoSelecionado) {
        SelenideElement elemento = $("button[value='" + elementoSelecionado + "']");
        elemento.click();
    }

    private static void selecionarDadosComValue(String elementoSelecionado) {
        // Localiza o elemento de botão com base no valor passado (sim ou não)
        SelenideElement elemento = $("[value='" + elementoSelecionado + "']");

        // Clica no elemento
        elemento.click();
    }
}
