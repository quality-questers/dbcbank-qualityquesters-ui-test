package org.dbcbank.page.cliente.perfil;

import com.codeborne.selenide.SelenideElement;
import org.dbcbank.data.dto.cliente.perfil.PerfilDadosPessoaisDTO;
import org.dbcbank.page.BasePage;
import org.dbcbank.selectors.cliente.perfil.PerfilDadosPessoaisSelectors;

import static com.codeborne.selenide.Selenide.$;

public class PerfilDadosPessoaisPage extends BasePage {
    public void limparCampos() {
        limparInput(PerfilDadosPessoaisSelectors.INPUT_NAME);
        limparInput(PerfilDadosPessoaisSelectors.INPUT_DATA_NASCIMENTO);
        limparInput(PerfilDadosPessoaisSelectors.INPUT_CEP);
        limparInput(PerfilDadosPessoaisSelectors.INPUT_BAIRRO);
        limparInput(PerfilDadosPessoaisSelectors.INPUT_LOGRADOURO);
        limparInput(PerfilDadosPessoaisSelectors.INPUT_NUMERO);
        limparInput(PerfilDadosPessoaisSelectors.INPUT_COMPLEMENTO);
    }

    public void setBtnSalvarDadosPessoais(){
        clicar(PerfilDadosPessoaisSelectors.BTN_SALVAR_DADOS_PESSOAIS);
    }

    public void alterarDadosPessoais(PerfilDadosPessoaisDTO perfil){
        preencherInputText(PerfilDadosPessoaisSelectors.INPUT_NAME, perfil.getNomeCompleto());
        selecionarDadosComValue(perfil.getPronome());
        selecionarDadosComValue(perfil.getNaturalidade());
        preencherInputText(PerfilDadosPessoaisSelectors.INPUT_DATA_NASCIMENTO, perfil.getDataDeNascimento());
        preencherInputText(PerfilDadosPessoaisSelectors.INPUT_CEP, perfil.getCep());
        preencherInputText(PerfilDadosPessoaisSelectors.INPUT_BAIRRO, perfil.getBairro());
        preencherInputText(PerfilDadosPessoaisSelectors.INPUT_LOGRADOURO, perfil.getLogradouro());
        preencherInputText(PerfilDadosPessoaisSelectors.INPUT_NUMERO, perfil.getNumero());
        preencherInputText(PerfilDadosPessoaisSelectors.INPUT_COMPLEMENTO, perfil.getComplemento());
        selecionarDadosComValue("RS");
        clicar(PerfilDadosPessoaisSelectors.ABRIR_LISTA_CIDADE);
        selecionarDadosDaCidade(perfil.getCidade());
    }

    private static void selecionarDadosDaCidade(String cidade) {
        // Localiza o campo de pesquisa da cidade e insira o nome da cidade
        SelenideElement campoPesquisaCidade = $("input[placeholder='Pesquisar...']");
        preencherInputText(campoPesquisaCidade, cidade);

        // Localiza a lista de opções
        SelenideElement listaOpcoes = $("div[role='presentation'][cmdk-group]");
        if (listaOpcoes.exists()) {
            // Localiza a opção correspondente à cidade desejada na lista de opções
            SelenideElement opcaoCidade = listaOpcoes.$("[data-value='" + cidade + "']");
            if (opcaoCidade.exists()) {
                // Clica na opção da cidade
                opcaoCidade.click();
            } else {
                System.out.println("Cidade '" + cidade + "' não encontrada na lista de opções.");
            }
        } else {
            System.out.println("Lista de opções não encontrada.");
        }
    }

    private static void selecionarDadosComValue(String elementoSelecionado) {
        // Localiza o elemento de botão com base no valor passado (sim ou não)
        SelenideElement elemento = $("[value='" + elementoSelecionado + "']");

        // Clica no elemento
        elemento.click();
    }
}
