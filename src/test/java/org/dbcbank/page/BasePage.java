package org.dbcbank.page;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;

import static com.codeborne.selenide.Condition.*;
import static org.testng.Assert.assertTrue;


public class BasePage {
    protected static void preencherInputText(SelenideElement element, String text) {
        element.val(text);
    }

    protected static void preencherInputNumero(SelenideElement element, String text) {
        element.setValue(text).shouldBe(editable);
    }

    protected static void selecionarInput(SelenideElement element, String text) {
        element.selectOptionByValue(text);
    }

    protected static void clicar(SelenideElement element) {
        element.click();
    }

    protected static void clicarDeNovo(SelenideElement element, String text) {
        element.doubleClick().selectOptionContainingText(text);
    }

    protected static void estaVisivel(SelenideElement element) {
        element.shouldBe(Condition.visible);
    }

    protected static void clicarNoMenuLateral(SelenideElement element) {
        element.shouldBe(clickable).click();
    }

    protected static void verificarTexto(SelenideElement element, String text) {
        element.shouldHave(text(text)).shouldBe(visible);
    }

    protected static boolean verificarTextoAssertion(SelenideElement element, String text) {
        return element.shouldHave(text(text)).exists();
    }

    protected static void limparInput(SelenideElement element) {
        element.clear();
    }

    public void validarUrlDaPagina(String expectedUrl) {
        String url = WebDriverRunner.url();
        assertTrue(url.contains(expectedUrl), "A URL da página não contém o valor esperado: " + expectedUrl);
    }

    protected static String obterValorElemento(SelenideElement element) {
        return element.getText();
    }
}
