package org.dbcbank.data.factory.datafactory.transacoes.transferencia.ted;

import org.dbcbank.data.dto.transacoes.transferencia.ted.TransferenciaTedDTO;
import org.dbcbank.rest.controller.UsuarioController;
import org.dbcbank.rest.dto.usuario.UsuarioDadosCompletoModel;
import org.dbcbank.util.user.UserData;

import java.math.BigDecimal;

public class TransferenciaTedDataFactory {
    private TransferenciaTedDataFactory() {
        // Empty constructor
    }

    private static String destinatarioDocument;
    private static String destinatarioAccount;
    private static String destinatarioBranchId;

    private static void obtemDadosDoDestinatario() {
        String token = UsuarioController.logar(UserData.loginUsuarioDestinatario());
        UsuarioDadosCompletoModel usuario = UsuarioController.buscarUsuario(token);
        destinatarioAccount = usuario.getBankAccount().getAccNumber();
        destinatarioBranchId = usuario.getBankAccount().getBranchId();
        destinatarioDocument = usuario.getDocument();
    }

    public static TransferenciaTedDTO transferenciaValida() {
        obtemDadosDoDestinatario();
        TransferenciaTedDTO transferencia = new TransferenciaTedDTO();

        transferencia.setAmount(BigDecimal.valueOf(1000.00));
        transferencia.setDestinationBranchId(destinatarioBranchId);
        transferencia.setRecipientDocument(destinatarioDocument);
        transferencia.setDestinationAccNumber(destinatarioAccount);
        transferencia.setTransactionDescription("Transferência de Teste.");

        return transferencia;
    }

    public static TransferenciaTedDTO transferenciaParaContaInvalida() {
        obtemDadosDoDestinatario();
        TransferenciaTedDTO transferencia = new TransferenciaTedDTO();

        transferencia.setAmount(BigDecimal.valueOf(1000.00));
        transferencia.setDestinationBranchId(destinatarioBranchId);
        transferencia.setRecipientDocument(destinatarioDocument);
        transferencia.setDestinationAccNumber("12345-6");
        transferencia.setTransactionDescription("Transferência de Teste com conta errada.");

        return transferencia;
    }

}
