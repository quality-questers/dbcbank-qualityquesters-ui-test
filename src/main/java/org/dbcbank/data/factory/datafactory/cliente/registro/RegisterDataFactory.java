package org.dbcbank.data.factory.datafactory.cliente.registro;

import org.dbcbank.data.dto.cliente.registro.RegisterDTO;
import org.dbcbank.util.faker.DataFakerGenerator;

public class RegisterDataFactory {
    private RegisterDataFactory() {
        // Empty constructor
    }

    public static RegisterDTO cadastroDeUsuarioComDadosValidos(){
        RegisterDTO registerDTO = new RegisterDTO();

        String email = DataFakerGenerator.emailGenerator();
        String senhaAcesso = DataFakerGenerator.senhaAcessoGenerator();
        String senhaTransacao = DataFakerGenerator.senhaTransacaoGenerator();

        registerDTO.setNomeCompleto(DataFakerGenerator.nomeGenerator());
        registerDTO.setCpf(DataFakerGenerator.cpfGenerator());
        registerDTO.setCelular(DataFakerGenerator.celularGenerator());
        registerDTO.setBirthdate(DataFakerGenerator.dataNascimentoGenerator());
        registerDTO.setEmail(email);
        registerDTO.setConfirmarEmail(email);
        registerDTO.setPassword(senhaAcesso);
        registerDTO.setPasswordConfirmacao(senhaAcesso);
        registerDTO.setSenhaTransacao(senhaTransacao);
        registerDTO.setConfirmarSenhaTransacao(senhaTransacao);

        return registerDTO;
    }

    public static RegisterDTO usuarioCampoNome(String nome){
        RegisterDTO usuario = RegisterDataFactory.cadastroDeUsuarioComDadosValidos();
        usuario.setNomeCompleto(nome);
        return usuario;
    }

    public static RegisterDTO usuarioCampoCPF(String document){
        RegisterDTO usuario = RegisterDataFactory.cadastroDeUsuarioComDadosValidos();
        usuario.setCpf(document);
        return usuario;
    }

    public static RegisterDTO usuarioCampoSenha(String senha){
        RegisterDTO usuario = RegisterDataFactory.cadastroDeUsuarioComDadosValidos();
        usuario.setPassword(senha);
        usuario.setPasswordConfirmacao(senha);
        return usuario;
    }

    public static RegisterDTO usuarioEmail(String email){
        RegisterDTO usuario = RegisterDataFactory.cadastroDeUsuarioComDadosValidos();
        usuario.setEmail(email);
        usuario.setConfirmarEmail(email);
        return usuario;
    }
}
