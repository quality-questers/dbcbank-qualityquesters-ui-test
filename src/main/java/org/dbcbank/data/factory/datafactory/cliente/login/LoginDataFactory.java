package org.dbcbank.data.factory.datafactory.cliente.login;

import org.dbcbank.data.dto.cliente.login.LoginDTO;
import org.dbcbank.util.config.ConfigProperties;
import org.dbcbank.util.user.UserData;

public class LoginDataFactory {
    private LoginDataFactory() {
        // Empty constructor
    }

    private static LoginDTO criarLoginDTO(String username, String password) {
        LoginDTO loginDTO = new LoginDTO();
        loginDTO.setCpf(ConfigProperties.getProperties().getProperty(username));
        loginDTO.setSenha(ConfigProperties.getProperties().getProperty(password));

        return loginDTO;
    }

    public static LoginDTO loginDadosValidos() {
        return criarLoginDTO(UserData.VALID_USERNAME, UserData.VALID_PASSWORD);
    }

    public static LoginDTO loginUsuarioRemetente(){
        return criarLoginDTO(UserData.USUARIO_REMETENTE, UserData.PASSWORD_USUARIO_REMETENTE);
    }

    public static LoginDTO loginUsuarioRemetenteTransferenciaPix(){
        return criarLoginDTO(UserData.USERNAME_REMETENTE_TRANSFERENCIA_PIX, UserData.PASSWORD_REMETENTE_TRANSFERENCIA_PIX);
    }

    public static LoginDTO loginUsuarioDestinatario(){
        return criarLoginDTO(UserData.USUARIO_DESTINATARIO, UserData.PASSWORD_USUARIO_DESTINATARIO);
    }

    public static LoginDTO loginComUsuarioEmBrancoESenhaValida() {
        return criarLoginDTO(UserData.BLANK_USERNAME, UserData.VALID_PASSWORD);
    }

    public static LoginDTO loginComUsuarioValidoESenhaInvalida() {
        return criarLoginDTO(UserData.VALID_USERNAME, UserData.INVALID_PASSWORD);
    }

    public static LoginDTO loginComUsuarioInvalidoESenhaValida() {
        return criarLoginDTO(UserData.INVALID_USERNAME, UserData.VALID_PASSWORD);
    }

    public static LoginDTO loginComUsuarioLimite() {
        return criarLoginDTO(UserData.USUARIO_LIMITE, UserData.PASSWORD_USUARIO_LIMITE);
    }
}
