package org.dbcbank.data.factory.datafactory.transacoes.transferencia.pix;

import org.dbcbank.data.dto.transacoes.transferencia.pix.TransferenciaPixDTO;
import org.dbcbank.util.config.ConfigProperties;

import java.math.BigDecimal;
import java.security.SecureRandom;

public class TransferenciaPixDataFactory {

    private TransferenciaPixDataFactory() {
        // Classe com métodos estáticos
    }

    private static final String CHAVES_PIX_DESTINATARIO = "destinatario.keys.transferencia.pix";
    private static final SecureRandom RANDOM = new SecureRandom();

    public static TransferenciaPixDTO gerarTransferenciaValida() {
        TransferenciaPixDTO transferencia = new TransferenciaPixDTO();

        int transferValue = RANDOM.nextInt(299) + 1;

        transferencia.setAmount(BigDecimal.valueOf(transferValue));
        transferencia.setDestinationKey(escolherUmaChavePixValidaDestinatarioAleatoriamente());

        return transferencia;
    }

    private static String escolherUmaChavePixValidaDestinatarioAleatoriamente() {
        String[] chavesDestinatario = ConfigProperties.getProperties().getProperty(CHAVES_PIX_DESTINATARIO).split(",");
        return chavesDestinatario[RANDOM.nextInt(chavesDestinatario.length)];
    }
}
