package org.dbcbank.data.factory.datafactory.transacoes.boleto;

import net.datafaker.Faker;

public class BoletoDataFactory {

    private static final Faker FAKER = new Faker();

    private BoletoDataFactory() {
        // classe com métodos estáticos
    }

    public static String gerarTextoAleatorioCampoDecricao() {
        return
                FAKER.lorem().characters(1, 10, true) +
                        " " +
                        FAKER.lorem().characters(1, 10, true) +
                        " " +
                        FAKER.lorem().characters(1, 10, true) +
                        " " +
                        FAKER.lorem().characters(1, 10, true) +
                        " " +
                        FAKER.lorem().characters(1, 10, true) +
                        " " +
                        FAKER.lorem().characters(1, 10, true) +
                        " " +
                        FAKER.lorem().characters(1, 10, true) +
                        " " +
                        FAKER.lorem().characters(1, 10, true) +
                        " " +
                        FAKER.lorem().characters(1, 10, true) +
                        " " +
                        FAKER.lorem().characters(1, 10, true)
                ;
    }
}
