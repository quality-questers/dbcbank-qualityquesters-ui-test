package org.dbcbank.data.factory.datafactory.cliente.perfil;

import org.dbcbank.data.dto.cliente.perfil.PerfilDadosPessoaisDTO;
import org.dbcbank.util.faker.DataFakerGenerator;

public class PerfilDadosPessoaisDataFactory {


    public PerfilDadosPessoaisDTO alteracaoDeDadosPessoais(){
        PerfilDadosPessoaisDTO perfil = new PerfilDadosPessoaisDTO();

        perfil.setNomeCompleto(DataFakerGenerator.nomeGenerator());
        perfil.setDataDeNascimento(DataFakerGenerator.dataNascimentoGenerator());
        perfil.setPronome(DataFakerGenerator.pronomeGenerator());
        perfil.setNaturalidade(DataFakerGenerator.naturalidadeGenerator());
        perfil.setCidade(DataFakerGenerator.cidadeGenerator());
        perfil.setCep(DataFakerGenerator.cepGenerator());
        perfil.setBairro(DataFakerGenerator.bairroGenerator());
        perfil.setLogradouro(DataFakerGenerator.logradouroGenerator());
        perfil.setNumero(DataFakerGenerator.numeroGenerator());
        perfil.setComplemento(DataFakerGenerator.complementoGenerator());

        return perfil;
    }
}
