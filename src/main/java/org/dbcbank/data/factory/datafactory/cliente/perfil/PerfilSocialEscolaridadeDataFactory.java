package org.dbcbank.data.factory.datafactory.cliente.perfil;

import org.dbcbank.data.dto.cliente.perfil.PerfilSocialEscolaridadeDTO;
import org.dbcbank.util.faker.DataFakerGenerator;

import java.security.SecureRandom;


public class PerfilSocialEscolaridadeDataFactory {
    public PerfilSocialEscolaridadeDTO alteracaoSocialEscolaridade() {
        PerfilSocialEscolaridadeDTO perfil = new PerfilSocialEscolaridadeDTO();
        perfil.setGenero(DataFakerGenerator.generoGenerator());
        perfil.setAutodeclaracaoRacial(DataFakerGenerator.racaGenerator());
        // Selecionar deficiência com base no resultado de Math.random()
        SecureRandom random = new SecureRandom();

        boolean isPcd = random.nextBoolean();
        perfil.setDeficiencia(isPcd ? "sim" : "não");

        // Definir descrição se houver deficiência
        if (isPcd) {
            String pcdDescription = DataFakerGenerator.gerarDescricao();
            perfil.setDescricao(pcdDescription.substring(0, Math.min(50, pcdDescription.length())));
        } else {
            perfil.setDescricao(null);
        }

        perfil.setRendaMensal(DataFakerGenerator.rendaMensalGenerator());
        perfil.setGrauDeEscolaridade(DataFakerGenerator.escolaridadeGenerator());
        perfil.setRegimeDeTrabalho(DataFakerGenerator.tipoContratoGenerator());
        perfil.setProfissao(DataFakerGenerator.gerarProfissao());
        perfil.setTempoDeProfissao(DataFakerGenerator.experienciaGenerator());

        return perfil;
    }
}

