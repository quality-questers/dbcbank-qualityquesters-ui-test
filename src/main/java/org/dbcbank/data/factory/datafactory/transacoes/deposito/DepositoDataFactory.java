package org.dbcbank.data.factory.datafactory.transacoes.deposito;

import org.dbcbank.data.dto.transacoes.deposito.DepositoDTO;
import org.dbcbank.rest.controller.UsuarioController;
import org.dbcbank.rest.dto.usuario.UsuarioDadosCompletoModel;
import org.dbcbank.util.faker.DataFakerGenerator;
import org.dbcbank.util.user.UserData;

public class DepositoDataFactory {
    private DepositoDataFactory() {
        // Empty constructor
    }

    public static DepositoDTO realizarDeposito() {
        String token = UsuarioController.logar(UserData.loginUsuarioDestinatario());
        UsuarioDadosCompletoModel usuario = UsuarioController.buscarUsuario(token);
        String numeroContaDestinatario = usuario.getBankAccount().getAccNumber();
        String cpfDestinatario = usuario.getDocument();

        DepositoDTO depositar = new DepositoDTO();
        depositar.setAccNumber(numeroContaDestinatario);
        depositar.setCpf(cpfDestinatario);
        depositar.setAmount(DataFakerGenerator.gerarValorParaDeposito());

        return depositar;
    }
}
