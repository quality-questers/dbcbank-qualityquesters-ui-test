package org.dbcbank.data.factory.datafactory.transacoes.saque;

import org.dbcbank.data.dto.transacoes.saque.SaqueDTO;
import org.dbcbank.rest.controller.UsuarioController;
import org.dbcbank.util.user.UserData;

import java.math.BigDecimal;

public class SaqueDataFactory {
    private SaqueDataFactory() {
        // Empty constructor
    }

    public static SaqueDTO realizarSaqueValido() {
        SaqueDTO saque = new SaqueDTO();
        saque.setAmount(BigDecimal.valueOf(200.00));

        return saque;
    }

    public static SaqueDTO realizarSaqueAcimaDoValorEmConta() {
        String token = UsuarioController.logar(UserData.loginUsuarioRemetente());
        SaqueDTO saque = new SaqueDTO();

        saque.setAmount(UsuarioController.buscarUsuario(token).getBankAccount().getBalance().add(BigDecimal.valueOf(100)));
        return saque;
    }

}
