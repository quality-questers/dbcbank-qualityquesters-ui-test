package org.dbcbank.data.factory.datafactory.transacoes.caixinha;

import org.dbcbank.data.dto.transacoes.caixinha.DinheiroCaixinhaDTO;
import org.dbcbank.util.faker.DataFakerGenerator;

public class DinheiroCaixinhaDataFactory {
    private DinheiroCaixinhaDataFactory(){
        // Empty constructor
    }

    public static DinheiroCaixinhaDTO dinheiroCaixinha(){
        DinheiroCaixinhaDTO dinheiroCaixinha = new DinheiroCaixinhaDTO();

        dinheiroCaixinha.setAmount(DataFakerGenerator.gerarValorParaDeposito());

        return dinheiroCaixinha;
    }
}
