package org.dbcbank.data.factory.datafactory.transacoes.chavepix;

import org.dbcbank.enums.TipoChavePix;

import java.security.SecureRandom;

public class ChavePixDataFactory {
    private static final SecureRandom RANDOM = new SecureRandom();

    private ChavePixDataFactory() {
        // Classe com métodos estáticos
    }

    public static TipoChavePix gerarOpcoesChavePix() {
        return TipoChavePix.values()[RANDOM.nextInt(TipoChavePix.values().length)];
    }
}
