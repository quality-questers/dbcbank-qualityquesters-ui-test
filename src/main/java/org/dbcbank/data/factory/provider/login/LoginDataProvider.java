package org.dbcbank.data.factory.provider.login;

import org.dbcbank.data.factory.datafactory.cliente.login.LoginDataFactory;
import org.testng.annotations.DataProvider;

public class LoginDataProvider {

    public static final String CPF_OU_SENHA_INVALIDOS = "Cpf ou senha inválidos";

    private LoginDataProvider(){
        //empty constructor
    }

    @DataProvider(name = "provideLoginArguments")
    public static Object[][] provideLoginArguments() {
        return new Object[][] {
                {LoginDataFactory.loginComUsuarioEmBrancoESenhaValida(), CPF_OU_SENHA_INVALIDOS},
                {LoginDataFactory.loginComUsuarioValidoESenhaInvalida(), CPF_OU_SENHA_INVALIDOS},
                {LoginDataFactory.loginComUsuarioInvalidoESenhaValida(), CPF_OU_SENHA_INVALIDOS},
        };
    }
}
