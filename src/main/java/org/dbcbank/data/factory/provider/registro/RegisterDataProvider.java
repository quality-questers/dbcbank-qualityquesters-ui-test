package org.dbcbank.data.factory.provider.registro;

import org.dbcbank.data.factory.datafactory.cliente.registro.RegisterDataFactory;
import org.dbcbank.util.faker.DataFakerGenerator;
import org.testng.annotations.DataProvider;


public class RegisterDataProvider {

    private RegisterDataProvider() {
        // Empty constructor
    }

    @DataProvider(name = "TesteNegativosCampoNome")
    public static Object[][] testeNegativosCampoNome() {
        final String MENSAGEM_CARACTERES_PERMITIDOS = "O nome pode conter apenas letras, hífens (-) e apóstrofos (').";
        final String MENSAGEM_TAMANHO_INCORRETO = "O nome deve ter entre 5 e 100 caracteres";

        return new Object[][]{
                {RegisterDataFactory.usuarioCampoNome("Robs-@CADA@@@@"), MENSAGEM_CARACTERES_PERMITIDOS}, //Validar nome completo com nome menor que 5 caracteres (4)
                {RegisterDataFactory.usuarioCampoNome("Robs"), MENSAGEM_TAMANHO_INCORRETO}, //Validar nome completo com nome menor que 5 caracteres (4)
        };
    }

    @DataProvider(name = "TesteNegativosCampoCpf")
    public static Object[][] testeNegativosCampoCpf() {
        final String MENSAGEM_CPF_INVALIDO = "CPF inválido";

        return new Object[][]{
//                {RegisterDataFactory.usuarioCampoCPF(StringUtils.EMPTY), MENSAGEM_CPF_INVALIDO}, // Validar campo CPF vazio
                {RegisterDataFactory.usuarioCampoCPF("1" + DataFakerGenerator.cpfGenerator()), MENSAGEM_CPF_INVALIDO}, // Validar CPF com caracteres acima do permitido (12)
                {RegisterDataFactory.usuarioCampoCPF("@" + DataFakerGenerator.gerarDezNumeros()), MENSAGEM_CPF_INVALIDO}, // Validar CPF com caracteres especiais
                {RegisterDataFactory.usuarioCampoCPF(" " + DataFakerGenerator.gerarDezNumeros()), MENSAGEM_CPF_INVALIDO}, // Validar CPF em branco
                {RegisterDataFactory.usuarioCampoCPF("A" + DataFakerGenerator.gerarDezNumeros()), MENSAGEM_CPF_INVALIDO}, // Validar CPF com letras
                {RegisterDataFactory.usuarioCampoCPF(DataFakerGenerator.gerarDezNumeros()), MENSAGEM_CPF_INVALIDO},
        };
    }

    @DataProvider(name = "ValidacoesSimplesDoCampoSenha")
    public static Object[][] validacoesSimplesDoCampoSenha() {
        final String MENSAGEM_TAMANHO_INCORRETO = "A senha de login deve ter entre 8 e 16 caracteres.";
        final String MENSAGEM_PADROES_SIMPLES = "A senha de login deve conter pelo menos uma letra minúscula, uma letra maiúscula, um número e um caractere especial.";

        return new Object[][]{
                {RegisterDataFactory.usuarioCampoSenha("SkdosjSUdmfS@2024"), MENSAGEM_TAMANHO_INCORRETO}, // Validar inserção de senha com mais de 16 caracteres (17)
                {RegisterDataFactory.usuarioCampoSenha("Sk@2024"), MENSAGEM_TAMANHO_INCORRETO}, // Validar inserção de senha com menos de 8 caracteres (7)
                {RegisterDataFactory.usuarioCampoSenha("Senha1234@"), MENSAGEM_PADROES_SIMPLES},
                {RegisterDataFactory.usuarioCampoSenha("12345678qwerty@"), MENSAGEM_PADROES_SIMPLES},
                {RegisterDataFactory.usuarioCampoSenha("11111111@A"), MENSAGEM_PADROES_SIMPLES},
        };
    }
}
