package org.dbcbank.data.dto.cliente.registro;

import lombok.Data;

@Data
public class RegisterDTO {
    private String cpf;
    private String celular;
    private String email;
    private String confirmarEmail;
    private String nomeCompleto;
    private String password;
    private String passwordConfirmacao;
    private String senhaTransacao;
    private String birthdate;
    private String confirmarSenhaTransacao;
}
