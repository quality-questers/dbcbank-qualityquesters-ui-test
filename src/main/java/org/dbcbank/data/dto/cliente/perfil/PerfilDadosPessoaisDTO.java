package org.dbcbank.data.dto.cliente.perfil;

import lombok.Data;

@Data
public class PerfilDadosPessoaisDTO {
    private String nomeCompleto;
    private String dataDeNascimento;
    private String cpf;
    private String pronome;
    private String celular;
    private String naturalidade;
    private String cep;
    private String estado;
    private String cidade;
    private String bairro;
    private String logradouro;
    private String numero;
    private String complemento;
}
