package org.dbcbank.data.dto.cliente.perfil;

import lombok.Data;

@Data
public class PerfilSocialEscolaridadeDTO {
    private String genero;
    private String autodeclaracaoRacial;
    private String deficiencia;
    private String descricao;
    private String rendaMensal;
    private String grauDeEscolaridade;
    private String regimeDeTrabalho;
    private String profissao;
    private String tempoDeProfissao;
}
