package org.dbcbank.data.dto.cliente.login;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dbcbank.rest.dto.usuario.RegisterApiModel;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginDTO {
    private String cpf;
    private String senha;

    public LoginDTO(RegisterApiModel registro) {
        this.cpf = registro.getDocument();
        this.senha = registro.getLoginPwd();
    }
}
