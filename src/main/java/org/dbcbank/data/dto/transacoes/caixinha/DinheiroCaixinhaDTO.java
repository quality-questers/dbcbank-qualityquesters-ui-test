package org.dbcbank.data.dto.transacoes.caixinha;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class DinheiroCaixinhaDTO {
    private BigDecimal amount;
}
