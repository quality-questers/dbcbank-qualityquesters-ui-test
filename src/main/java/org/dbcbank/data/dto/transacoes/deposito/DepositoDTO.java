package org.dbcbank.data.dto.transacoes.deposito;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DepositoDTO {
    private String accNumber;
    private String cpf;
    private BigDecimal amount;
}
