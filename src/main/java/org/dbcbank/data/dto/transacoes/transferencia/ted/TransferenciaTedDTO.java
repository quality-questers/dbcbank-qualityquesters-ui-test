package org.dbcbank.data.dto.transacoes.transferencia.ted;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TransferenciaTedDTO {
    private BigDecimal amount;
    private String destinationAccNumber;
    private String destinationBranchId;
    private String recipientDocument;
    private String transactionDescription;
}
