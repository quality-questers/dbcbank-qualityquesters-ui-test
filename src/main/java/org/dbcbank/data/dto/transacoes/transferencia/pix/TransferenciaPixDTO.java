package org.dbcbank.data.dto.transacoes.transferencia.pix;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransferenciaPixDTO {
    private BigDecimal amount;
    private String destinationKey;
}