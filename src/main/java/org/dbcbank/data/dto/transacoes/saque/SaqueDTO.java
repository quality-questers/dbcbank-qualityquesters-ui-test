package org.dbcbank.data.dto.transacoes.saque;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class SaqueDTO {
    private BigDecimal amount;
}
