package org.dbcbank.util.user;

import org.dbcbank.rest.dto.usuario.LoginRequestModel;
import org.dbcbank.util.config.ConfigProperties;

import java.util.Properties;

public class UserData {
    private static final Properties props = ConfigProperties.getProperties();

    public static final String ADM_USERNAME = "valid.admin.username";
    public static final String ADM_PASSWORD = "valid.admin.password";
    public static final String VALID_USERNAME = "valid.username";
    public static final String VALID_PASSWORD = "valid.password";
    public static final String BLANK_PASSWORD = "blank.password";
    public static final String INVALID_PASSWORD = "invalid.password";
    public static final String BLANK_USERNAME = "blank.username";
    public static final String EMAIL = "email";
    public static final String INVALID_USERNAME = "invalid.username";
    public static final String USUARIO_LIMITE = "limit.username";
    public static final String PASSWORD_USUARIO_LIMITE = "limit.password";
    public static final String USUARIO_REMETENTE = "remetente.username";
    public static final String PASSWORD_USUARIO_REMETENTE = "remetente.password";
    public static final String USUARIO_DESTINATARIO = "destinatario.username";
    public static final String PASSWORD_USUARIO_DESTINATARIO = "destinatario.password";
    public static final String USUARIO_REMETENTE_PIX = "valid.username.remetente.transferencia.pix";
    public static final String PASSWORD_USUARIO_REMETENTE_PIX = "valid.password.remetente.transferencia.pix";
    public static final String USUARIO_DESTINATARIO_PIX = "valid.username.destinatario.transferencia.pix";
    public static final String PASSWORD_USUARIO_DESTINATARIO_PIX = "valid.password.destinatario.transferencia.pix";
    public static final String USERNAME_REMETENTE_TRANSFERENCIA_PIX = "valid.username.remetente.transferencia.pix";
    public static final String PASSWORD_REMETENTE_TRANSFERENCIA_PIX = "valid.password.remetente.transferencia.pix";
    public static final String VALID_TRANSACTION_PASSWORD_REMETENTE_PIX = "valid.transaction.password.remetente.transferencia.pix";

    private UserData() {
        // Empty constructor
    }

    public static String senhaTransacaoRemetentePix() {
        return ConfigProperties.getProperties().getProperty(VALID_TRANSACTION_PASSWORD_REMETENTE_PIX);
    }


    private static LoginRequestModel gerarLoginRequestByProperties(String usernameProperty, String passwordProperty) {
        return new LoginRequestModel(props.getProperty(usernameProperty), props.getProperty(passwordProperty));
    }

    public static LoginRequestModel loginUsuarioCredenciaisValidas() {
        return gerarLoginRequestByProperties(VALID_USERNAME, VALID_PASSWORD);
    }

    public static LoginRequestModel loginUsuarioAdmin() {
        return gerarLoginRequestByProperties(ADM_USERNAME, ADM_PASSWORD);
    }

    public static LoginRequestModel loginComUsuarioEmBrancoESenhaValida() {
        return gerarLoginRequestByProperties(BLANK_USERNAME, VALID_PASSWORD);
    }

    public static LoginRequestModel loginComUsuarioValidoESenhaEmBranco() {
        return gerarLoginRequestByProperties(VALID_USERNAME, BLANK_PASSWORD);
    }

    public static LoginRequestModel loginComUsuarioValidoESenhaInvalida() {
        return gerarLoginRequestByProperties(VALID_USERNAME, INVALID_PASSWORD);
    }

    public static LoginRequestModel loginComEmailAoInvesDeUsuario() {
        return gerarLoginRequestByProperties(props.getProperty(EMAIL), props.getProperty(VALID_PASSWORD));
    }

    public static LoginRequestModel loginComUsuarioInvalidoESenhaValida() {
        return gerarLoginRequestByProperties(props.getProperty(INVALID_USERNAME), props.getProperty(VALID_PASSWORD));
    }

    public static LoginRequestModel loginUsuarioLimite() {
        return gerarLoginRequestByProperties(USUARIO_LIMITE, PASSWORD_USUARIO_LIMITE);
    }

    public static LoginRequestModel loginUsuarioRemetente() {
        return gerarLoginRequestByProperties(USUARIO_REMETENTE, PASSWORD_USUARIO_REMETENTE);
    }

    public static LoginRequestModel loginUsuarioDestinatario() {
        return gerarLoginRequestByProperties(USUARIO_DESTINATARIO, PASSWORD_USUARIO_DESTINATARIO);
    }

    public static LoginRequestModel loginUsuarioRemetenteTransferenciaPix() {
        return gerarLoginRequestByProperties(USUARIO_REMETENTE_PIX, PASSWORD_USUARIO_REMETENTE_PIX);
    }

    public static LoginRequestModel loginUsuarioDestinatarioTransferenciaPix() {
        return gerarLoginRequestByProperties(USUARIO_DESTINATARIO_PIX, PASSWORD_USUARIO_DESTINATARIO_PIX);
    }

    public static LoginRequestModel gerarLoginRequest(String login, String password) {
        return new LoginRequestModel (login, password);
    }
}
