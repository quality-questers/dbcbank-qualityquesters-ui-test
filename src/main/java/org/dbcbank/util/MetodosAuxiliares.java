package org.dbcbank.util;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import static com.codeborne.selenide.Selectors.byClassName;
import static com.codeborne.selenide.Selenide.$$;

public class MetodosAuxiliares {
    private MetodosAuxiliares(){
        //Empty constructor
    }

    private static final ElementsCollection DIVS_SENHA = $$(byClassName("cursor-pointer"));

    public static void preencherSenhaDeTransacao(String senhaTransacao){

        for (char digito : senhaTransacao.toCharArray()) {
            selecionarDivPelaSenha(String.valueOf(digito));
        }
    }

    private static void selecionarDivPelaSenha(String senhaDeTransacao) {
        for (SelenideElement div : DIVS_SENHA) {
            String texto = div.getText();
            if (contemDigitoDaSenha(texto, senhaDeTransacao)) {
                div.click();
                break;
            }
        }
    }

    private static boolean contemDigitoDaSenha(String texto, String senhaDeTransacao) {
        for (char digito : senhaDeTransacao.toCharArray()) {
            if (texto.contains(String.valueOf(digito))) {
                return true;
            }
        }
        return false;
    }

    public static String decodeCustomerId(String token) {
        try {
            String[] splitString = token.split("\\.");
            if (splitString.length < 2) {
                throw new IllegalArgumentException("Token inválido: não contém duas partes.");
            }
            String base64EncodedBody = splitString[1];

            Base64.Decoder decoder = Base64.getUrlDecoder();
            String payload = new String(decoder.decode(base64EncodedBody), StandardCharsets.UTF_8);

            String[] parts = payload.split("\"customerId\":");
            String[] nextPart = parts[1].split(",");
            String numberStr = nextPart[0].trim();
            numberStr = numberStr.replaceAll("\\D", "");

            return numberStr;
        } catch (Exception e) {
            throw new IllegalArgumentException("Erro ao decodificar o token: " + e.getMessage(), e);
        }
    }
}
