package org.dbcbank.util.faker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Constants {
    private Constants(){
        //Empty Constants
    }

    protected static final List<String> GENEROS = new ArrayList<>(Arrays.asList("Homem Cis", "Mulher Cis", "Homem Trans", "Mulher Trans", "Outra identidade de gênero"));
    protected static final List<String> RACA = new ArrayList<>(Arrays.asList("Branca", "Preta", "Parda", "Amarela", "Indígena", "Prefiro não responder"));
    protected static final List<String> ESCOLARIDADE = new ArrayList<>(Arrays.asList("Mestrado", "Pós Graduação Completa", "Pós Graduação Incompleta", "Ensino Superior Completo", "Ensino Superior Incompleto", "Ensino Médio Completo", "Ensino Médio Incompleto", "Ensino Fundamental Completo", "Ensino Fundamental Incompleto"));
    protected static final List<String> RENDA_MENSAL = new ArrayList<>(Arrays.asList("Até R$1.100,00", "Entre R$1.100,00 e R$2.200,00", "Entre R$2.200,00 e R$3.300,00", "Entre R$3.300,00 e R$5.500,00", "Acima de R$5.500,00"));
    protected static final List<String> EXPERIENCIA = new ArrayList<>(Arrays.asList("Até 1 ano", "Entre 1 e 3 anos", "Entre 3 e 5 anos", "Acima de 5 anos"));
    protected static final List<String> TIPO_CONTRATO = new ArrayList<>(Arrays.asList("contrato por tempo determinado", "contrato CLT", "contrato de trabalho eventual", "contrato de estágio", "contrato de experiência", "contrato de teletrabalho", "contrato intermitente", "contrato de trabalho autônomo"));
    protected static final List<String> PRONOMES = new ArrayList<>(Arrays.asList("ele/dele", "ela/dela", "elu/delu", "eles/deles"));
    protected static final List<String> NATURALIDADES = Arrays.asList(
            "Brasil", "Antígua e Barbuda", "Argentina", "Bahamas", "Barbados", "Belize", "Bolívia", "Chile",
            "Colômbia", "Costa Rica", "Cuba", "Dominica", "Equador", "El Salvador", "Granada",
            "Guatemala", "Guiana", "Guiana Francesa", "Haiti", "Honduras", "Jamaica", "México", "Nicarágua",
            "Panamá", "Paraguai", "Peru", "Porto Rico", "República Dominicana", "São Cristóvão e Nevis", "São Vicente e Granadinas",
            "Santa Lúcia", "Suriname", "Trinidad e Tobago", "Uruguai", "Venezuela", "Alemanha", "Áustria", "Bélgica",
            "Croácia", "Dinamarca", "Eslováquia", "Eslovênia", "Espanha", "França", "Grécia", "Hungria", "Irlanda",
            "Itália", "Noruega", "Países Baixos", "Polônia", "Portugal", "Reino Unido", "Inglaterra", "País de Gales", "Escócia",
            "Romênia", "Rússia", "Sérvia", "Suécia", "Suíça", "Turquia", "Ucrânia", "Estados Unidos", "Canadá", "Angola",
            "Moçambique", "África do Sul", "Zimbabue", "Argélia", "Comores", "Egito", "Líbia", "Marrocos",
            "Gana", "Quênia", "Ruanda", "Uganda", "Botsuana", "Costa do Marfim", "Camarões", "Nigéria", "Somália",
            "Austrália", "Nova Zelândia", "Afeganistão", "Arábia Saudita", "Armênia", "Bangladesh", "China", "Coréia do Norte",
            "Coréia do Sul", "Índia", "Indonésia", "Iraque", "Irã", "Israel", "Japão", "Malásia", "Nepal",
            "Omã", "Paquistão", "Palestina", "Qatar", "Síria", "Sri Lanka", "Tailândia", "Timor-Leste", "Emirados Árabes Unidos",
            "Vietnã", "Iêmen"
    );

    protected static final List<String> ESTADOS = Arrays.asList(
            "Acre", "Alagoas", "Amapá", "Amazonas", "Bahia", "Ceará", "Distrito Federal", "Espírito Santo", "Goiás", "Maranhão", "Mato Grosso", "Mato Grosso do Sul",
            "Minas Gerais", "Pará", "Paraíba", "Paraná", "Pernambuco", "Piauí", "Rio de Janeiro", "Rio Grande do Norte", "Rio Grande do Sul", "Rondônia",
            "Roraima", "Santa Catarina", "São Paulo", "Sergipe", "Tocantins"
    );

    protected static final List<String> CIDADES = Arrays.asList("aceguá", "água santa", "agudo", "ajuricaba", "alecrim", "alegrete",
            "alegria", "almirante tamandaré do sul", "alpestre", "alto alegre", "alto feliz",  "alvorada", "amaral ferrador", "ametista do sul");
}
