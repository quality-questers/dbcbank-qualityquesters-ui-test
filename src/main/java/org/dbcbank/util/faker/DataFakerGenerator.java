package org.dbcbank.util.faker;

import net.datafaker.Faker;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class DataFakerGenerator {
    public static final Faker faker = new Faker(new Locale("PT-BR"));

    private DataFakerGenerator() {
        // Empty constructor
    }

    // Gera um CPF válido
    public static String cpfGenerator() {
        return faker.cpf().valid(false);
    }

    // Gera um celular válido para cadastro de um novo usuário
    public static String celularGenerator() {
        return faker.phoneNumber().phoneNumberNational();
    }

    // Gera um nome válido
    public static String nomeGenerator() {
        return faker.name().fullName().replace(".", "");
    }

    // Gera um email válido
    public static String emailGenerator() {
        return faker.number().numberBetween(0, 999) + faker.internet().emailAddress();
    }

    // Gera uma senha válida
    public static String senhaAcessoGenerator() {
        String fakerPassword = faker.internet().password();

        return "@Ab2" + fakerPassword.substring(0, Math.min(fakerPassword.length(), 12));
    }

    // Gera uma senha de transação válida
    public static String senhaTransacaoGenerator() {
        return faker.numerify("####");
    }

    // Gera números
    public static String gerarDezNumeros() {
        return faker.numerify("##########");
    }

    // Gera um valor aleatório (dinheiro)
    public static double gerarValorRange(int valorMin, int valorMax, int cadasDecimais) {
        return faker.number().randomDouble(cadasDecimais, valorMin, valorMax);
    }

    // Gera uma data aleatória
    public static String dataNascimentoGenerator() {
        Date dataNascimento = faker.date().birthday(18, 120);
        SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy");
        return formatoData.format(dataNascimento);
    }

    // Retorna um CEP aleatório
    public static String cepGenerator() {
        String cepComTraco = faker.address().zipCode();
        // Remover o traço do CEP gerado
        return cepComTraco.replace("-", "");
    }

    // Retorna um prefixo de bairro aleatório
    public static String bairroGenerator() {
        return faker.address().cityPrefix();
    }

    // Retorna um nome de rua aleatório
    public static String logradouroGenerator() {
        return faker.address().streetName();
    }

    // Retorna um número de endereço aleatório
    public static String numeroGenerator() {
        return faker.address().streetAddressNumber();
    }

    // Retorna um complemento de endereço aleatório
    public static String complementoGenerator() {
        return faker.address().secondaryAddress();
    }

    // Gera uma descrição aleatória de deficiência
    public static String gerarDescricao() {
        return faker.lorem().sentence();
    }

    // Gera uma profissão aleatória
    public static String gerarProfissao() {
        return faker.job().position();
    }

    public static BigDecimal gerarValorParaDeposito() {
        double valorDouble = faker.number().randomDouble(2, 1, 100);
        BigDecimal valor = BigDecimal.valueOf(valorDouble);
        return valor.setScale(2, RoundingMode.HALF_UP);
    }

    // Escolhe um pronome aleatório
    public static String pronomeGenerator() {
        return randomElement(Constants.PRONOMES);
    }

    // Escolhe naturalidades aleatórias
    public static String naturalidadeGenerator() {
        return randomElement(Constants.NATURALIDADES);
    }

    // Retorna um estado aleatório
    public static String estadoGenerator() {
        return randomElement(Constants.ESTADOS);
    }

    public static String generoGenerator() {
        return randomElement(Constants.GENEROS);
    }

    public static String racaGenerator() {
        return randomElement(Constants.RACA);
    }

    public static String escolaridadeGenerator() {
        return randomElement(Constants.ESCOLARIDADE);
    }

    public static String rendaMensalGenerator() {
        return randomElement(Constants.RENDA_MENSAL);
    }

    public static String experienciaGenerator() {
        return randomElement(Constants.EXPERIENCIA);
    }

    public static String tipoContratoGenerator() {
        return randomElement(Constants.TIPO_CONTRATO);
    }

    // Retorna uma cidade aleatória
    public static String cidadeGenerator() {
        return randomElement(Constants.CIDADES);
    }

    private static <T> T randomElement(List<T> list) {
        return list.get(faker.number().numberBetween(0, list.size()));
    }
}
