package org.dbcbank.rest.controller;

import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.dbcbank.data.factory.datafactory.cliente.registro.RegisterDataFactory;
import org.dbcbank.rest.dto.usuario.CartaoModel;
import org.dbcbank.rest.dto.usuario.LoginRequestModel;
import org.dbcbank.rest.dto.usuario.RegisterApiModel;
import org.dbcbank.rest.dto.usuario.UsuarioDadosCompletoModel;
import org.dbcbank.rest.specs.Specs;
import org.dbcbank.util.MetodosAuxiliares;
import org.dbcbank.util.user.UserData;

import static io.restassured.RestAssured.given;

public class UsuarioController {

    private static final String GET_DADOS_CLIENTE = "/customers/current";
    private static final String AUTHORIZATION = "Authorization";
    private static final String BEARER = "Bearer ";
    private static final String REGISTER = "/auth/register";
    private static final String LOGIN_ENDPOINT = "/auth/login";
    private static final String DELETE_ENDPOINT = "/admin/customers/{customerId}";
    private static final String CRIAR_CARTAO_CREDITO_ENDPOINT = "/accounts/cards/credit-card";

    private UsuarioController() {
        // Classe com métodos estáticos
    }

    public static RegisterApiModel cadastrarUsuario() {

        RegisterApiModel usuarioData = new RegisterApiModel(RegisterDataFactory.cadastroDeUsuarioComDadosValidos());

        given()
                .spec(Specs.specsAll())
                .body(usuarioData)
                .when()
                .post(REGISTER)
        ;

        return usuarioData;
    }

    public static UsuarioDadosCompletoModel buscarUsuario(String authToken) {
        return
                given()
                        .spec(Specs.specsAll())
                        .header(AUTHORIZATION, BEARER + authToken)
                        .when()
                        .get(GET_DADOS_CLIENTE)
                        .then()
                        .extract().as(UsuarioDadosCompletoModel.class)
                ;
    }

    public static String logar(LoginRequestModel loginData) {
        Response response =
                given()
                        .spec(Specs.specsAll())
                        .body(loginData)
                        .when()
                        .post(LOGIN_ENDPOINT);

        return response.getBody().path("token");
    }

    public static void excluirUsuario(String tokenDoUsuarioAhSerExcluido) {

        String idUsuario = MetodosAuxiliares.decodeCustomerId(tokenDoUsuarioAhSerExcluido);
        String admToken = logar(UserData.loginUsuarioAdmin());

        given()
                .spec(Specs.specsAll())
                .header(AUTHORIZATION, BEARER + admToken)
                .pathParams("customerId", idUsuario)
                .when()
                .delete(DELETE_ENDPOINT)
                .then()
                .statusCode(HttpStatus.SC_NO_CONTENT)
        ;
    }

    public static CartaoModel criarCartaoDeCredito(String authToken) {

        Response res =
                given()
                        .spec(Specs.specsAll())
                        .header(AUTHORIZATION, BEARER + authToken)
                        .when()
                        .post(CRIAR_CARTAO_CREDITO_ENDPOINT)
                        .then()
                        .statusCode(HttpStatus.SC_CREATED)
                        .extract().response();

        return res.jsonPath().getObject("creditCard", CartaoModel.class);
    }
}
