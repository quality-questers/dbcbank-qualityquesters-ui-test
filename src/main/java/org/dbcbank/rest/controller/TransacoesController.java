package org.dbcbank.rest.controller;

import org.dbcbank.data.dto.transacoes.saque.SaqueDTO;
import org.dbcbank.rest.dto.transacoes.DepositoRequestModel;
import org.dbcbank.rest.specs.Specs;

import java.math.BigDecimal;

import static io.restassured.RestAssured.given;

public class TransacoesController {
    private static final String DEPOSIT_ENDPOINT = "/transactions/deposit";
    private static final String AUTHORIZATION = "Authorization";
    private static final String BEARER = "Bearer ";

    private TransacoesController() {
        // Classe com métodos estáticos
    }

    public static void realizarDeposito(String cpf, String accNumber, Double valorDeposito, String authToken) {

        given()
                .spec(Specs.specsAll())
                .header(AUTHORIZATION, BEARER + authToken)
                .body(new DepositoRequestModel(cpf, accNumber, BigDecimal.valueOf(valorDeposito)))
                .when()
                .post(DEPOSIT_ENDPOINT)
        ;
    }

    public static void realizarSaque(SaqueDTO depositoData, String authToken) {
        given()
                .spec(Specs.specsAll())
                .header(AUTHORIZATION, BEARER + authToken)
                .body(depositoData)
                .when()
                .post(DEPOSIT_ENDPOINT);
    }
}
