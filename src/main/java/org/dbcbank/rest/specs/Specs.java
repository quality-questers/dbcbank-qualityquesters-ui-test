package org.dbcbank.rest.specs;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.config.LogConfig;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.config;

public class Specs {
    private static final String BASE_URL = "https://dbcbank-questers-back-vs4.onrender.com/dbc-bank/v1/";

    private Specs() {
    }

    public static RequestSpecification setUp(){

        return new RequestSpecBuilder()
                .setBaseUri(BASE_URL)
                .setConfig(config().logConfig(LogConfig.logConfig() .enableLoggingOfRequestAndResponseIfValidationFails()))
                .build();
    }

    public static RequestSpecification specsAll() {
        return new RequestSpecBuilder()
                .addRequestSpecification(setUp())
                .setContentType(ContentType.JSON)
                .build();
    }
}
