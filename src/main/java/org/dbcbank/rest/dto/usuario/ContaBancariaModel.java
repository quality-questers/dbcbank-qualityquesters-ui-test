package org.dbcbank.rest.dto.usuario;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ContaBancariaModel {
    private String accNumber;
    private String branchId;
    private String bankId;
    private BigDecimal balance;
    private BigDecimal savingsBalance;
    private int score;
    private String accType;
    private BigDecimal creditLimitMax;
    private BigDecimal creditLimitCurrent;
    private BigDecimal creditLimitUsed;
    private List<CartaoModel> card;
    private List<ChavePixModel> pixKeys;
}
