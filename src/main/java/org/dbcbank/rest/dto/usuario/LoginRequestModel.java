package org.dbcbank.rest.dto.usuario;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginRequestModel {

    private String username;
    private String password;

    public LoginRequestModel(RegisterApiModel usuario) {
        username = usuario.getDocument();
        password = usuario.getLoginPwd();
    }
}
