package org.dbcbank.rest.dto.usuario;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class UsuarioDadosCompletoModel {
    private String name;
    private String document;
    private String email;
    private String phoneNumber;
    private ContaBancariaModel bankAccount;
    private UsuarioModel user;
}
