package org.dbcbank.rest.dto.usuario;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.dbcbank.data.dto.cliente.registro.RegisterDTO;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegisterApiModel {

    public RegisterApiModel(RegisterDTO dados){
        name = dados.getNomeCompleto();
        document = dados.getCpf();
        email = dados.getEmail();
        emailConfirmation = dados.getEmail();
        phoneNumber = dados.getCelular().replace(" ","").replace("(", "").replace(")", "").replace("-", "");
        loginPwd = dados.getPassword();
        loginPwdConfirmation = dados.getPassword();
        transactionPwd = dados.getSenhaTransacao();
        transactionPwdConfirmation = dados.getSenhaTransacao();
        consentOpt = true;
        birthdate = dados.getBirthdate();
    }

    private String name;
    private String document;
    private String email;
    private String emailConfirmation;
    private String phoneNumber;
    private String loginPwd;
    private String loginPwdConfirmation;
    private String transactionPwd;
    private String transactionPwdConfirmation;
    private Boolean consentOpt;
    private String birthdate;
}
