package org.dbcbank.rest.dto.usuario;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CartaoModel {
    private String cardholderName;
    private String cardNumber;
    private String flag;
    private String expirationDate;
    private String securityCode;
    private String cardType;
    private Boolean isInternational;
    private Boolean isBlocked;
}
