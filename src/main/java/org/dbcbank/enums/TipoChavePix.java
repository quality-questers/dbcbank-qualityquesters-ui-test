package org.dbcbank.enums;

import lombok.Getter;

@Getter
public enum TipoChavePix {
    CPF("cpf", 1),
    TELEFONE("telefone", 2),
    EMAIL("email", 3),
    CHAVE_ALEATORIA("chave-aleatória", 4);

    private final String descricao;
    private final int codigo;

    TipoChavePix(String descricao, int codigo) {
        this.descricao = descricao;
        this.codigo = codigo;
    }
}
