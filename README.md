<div align="center">
  <img alt="Java" src="https://img.shields.io/badge/Java-Ff0000?style=for-the-badge&logo=coffeescript&logoColor=white">
  <img alt="selenide" src="https://img.shields.io/badge/Selenide-selenide?style=for-the-badge&logo=selenide&logoColor=white">
  <img alt="google" src="https://img.shields.io/badge/Google-EB493B?style=for-the-badge&logo=google&logoColor=white">
</div>

# Projeto de Teste de Automatizado de UI com Selenide - DBC Bank ☕

Repositório para alocar os testes automatizados de UI com Selenide que foram desenvolvidos para testar o frontend do DBC Bank.

## Link
- [DBC Bank Website](https://dbcbank-questers-front-vs1.onrender.com/login)

## Pré-requisitos ⚙️

- [IntelliJ Idea](https://www.jetbrains.com/idea/)
- [JDK17](https://www.oracle.com/java/technologies/downloads/)
- [Google Chrome](https://www.google.com/intl/pt-BR/chrome/)
- [Allure Report](https://allurereport.org/docs/junit4/)
- [Selenide](https://selenide.org/documentation.html)


## Dependências Utilizadas 👀

```pom.xml
 <properties>
        <sonar.organization>quality-questers</sonar.organization>
        <maven-failsafe-plugin.version>3.8.1</maven-failsafe-plugin.version>
        <maven-surefire-plugin.version>3.0.0-M5</maven-surefire-plugin.version>
        <java.version>17</java.version>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <testng.version>7.9.0</testng.version>
        <allure-junit4.version>2.19.0</allure-junit4.version>
        <allure-selenide.version>2.18.1</allure-selenide.version>
        <lombok.version>1.18.30</lombok.version>
        <aspectj.version>1.9.4</aspectj.version>
        <jackson.version>2.10.1</jackson.version>
        <jackson-datatype.version>2.17.0</jackson-datatype.version>
        <javafaker.version>1.0.2</javafaker.version>
        <selenide.version>7.2.3</selenide.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>com.codeborne</groupId>
            <artifactId>selenide</artifactId>
            <version>${selenide.version}</version>
        </dependency>
        <dependency>
            <groupId>org.testng</groupId>
            <artifactId>testng</artifactId>
            <version>${testng.version}</version>
        </dependency>
        <dependency>
            <groupId>io.qameta.allure</groupId>
            <artifactId>allure-junit4</artifactId>
            <version>${allure-junit4.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.datatype</groupId>
            <artifactId>jackson-datatype-jsr310</artifactId>
            <version>${jackson-datatype.version}</version>
        </dependency>
        <dependency>
            <groupId>com.github.javafaker</groupId>
            <artifactId>javafaker</artifactId>
            <version>${javafaker.version}</version>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>${lombok.version}</version>
        </dependency>
    </dependencies>
```

# Sistema de Testes Automatizados

## Ferramentas e Tecnologias Utilizadas

- **Selenide**: uma biblioteca de automação de testes em Java, construída sobre o Selenium WebDriver, que simplifica e torna mais fácil a escrita de testes automatizados para aplicativos web. Ele fornece uma API amigável e expressiva que simplifica a interação com elementos da página, aguarda automaticamente a conclusão de operações assíncronas e lida com várias complexidades de automação, permitindo que os desenvolvedores escrevam testes mais robustos e concisos. Selenide também oferece recursos úteis, como a capacidade de tirar screenshots automaticamente em caso de falha, integração com ferramentas de build e suporte para execução em diferentes navegadores de forma fácil e confiável. Selenide é amplamente utilizado na comunidade de automação de testes devido à sua simplicidade, eficácia e robustez.
- **Intellij IDEA**: IDE para Java e Kotlin, com suporte a recursos de linguagem.
- **Allure Report**: Ferramenta de código aberto para geração de relatórios de teste HTML.
- **Datafaker**: Biblioteca para Java e Kotlin para geraração de dados falsos.
- **TestNG**: Framework de teste para aplicações Java, inspirado no JUnit e NUnit.
- **SonarCloud**: uma plataforma de análise estática de código para avaliar a qualidade e segurança do código-fonte em projetos de software.

## Casos de Teste

#### 1. Login com dados válidos

#### 2. Registro de usuário com dados válidos
